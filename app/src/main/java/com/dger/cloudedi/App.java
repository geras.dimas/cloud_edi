package com.dger.cloudedi;

import android.app.Application;
import android.content.SharedPreferences;

import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.model.dto.CategoryDTO;
import com.dger.cloudedi.model.dto.DeliveryDTO;
import com.dger.cloudedi.model.dto.OrderStateDTO;
import com.dger.cloudedi.model.dto.SlipTypeDTO;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class App extends Application {
    private APIService apiService;
    private SharedPreferences profileSettings;
    private SharedPreferences.Editor editor;
    private static final String PREFS_PROFILE_FILE = "Profile";
    private static App instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(this);
        apiService = APIService.getInstance();
        getCategory();
        getDelivery();
        getOrderStates();
        getSipType();
        profileSettings = getSharedPreferences(PREFS_PROFILE_FILE, MODE_PRIVATE);
    }

    public static App get() {
        return instance;
    }

    public void getCategory() {
        apiService.getCategories().subscribe(categories -> setCategories(categories), error -> System.out.println(error));
    }

    public void getSipType() {
        apiService.getSlipTypes().subscribe(slipTypeDTOS -> setSlipTypes(slipTypeDTOS), error -> System.out.println(error));
    }

    public void getDelivery() {
        apiService.getDeliveries().subscribe(deliveryDTOS -> setDeliveries(deliveryDTOS), error -> System.out.println(error));
    }

    public void getOrderStates() {
        apiService.getOrderStates().subscribe(orderStateDTOS -> setOrderStates(orderStateDTOS), error -> System.out.println(error));
    }


    private void setCategories(List<CategoryDTO> categories) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            categories.forEach(category -> {

                Category categoryRealm = realm.where(Category.class).equalTo("id", category.getId()).findFirst();
                if (categoryRealm == null) {
                    categoryRealm = new Category();
                    categoryRealm.setId(category.getId());
                }
                categoryRealm.setName(category.getName());
                realm.copyToRealmOrUpdate(categoryRealm);

            });
            realm.commitTransaction();
            realm.close();

        }).start();
    }

    private void setSlipTypes(List<SlipTypeDTO> sliptypes) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            sliptypes.forEach(slipType -> {

                SlipType slipTypeRealm = realm.where(SlipType.class).equalTo("id", slipType.getId()).findFirst();
                if (slipTypeRealm == null) {
                    slipTypeRealm = new SlipType();
                    slipTypeRealm.setId(slipType.getId());
                }
                slipTypeRealm.setName(slipType.getName());
                realm.copyToRealmOrUpdate(slipTypeRealm);
            });
            realm.commitTransaction();
            realm.close();
        }).start();
    }

    private void setDeliveries(List<DeliveryDTO> deliveryDTOS) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            deliveryDTOS.forEach(delivery -> {

                Delivery deliveryRealm = realm.where(Delivery.class).equalTo("id", delivery.getId()).findFirst();
                if (deliveryRealm == null) {
                    deliveryRealm = new Delivery();
                    deliveryRealm.setId(delivery.getId());
                }
                deliveryRealm.setName(delivery.getName());
                realm.copyToRealmOrUpdate(deliveryRealm);
            });
            realm.commitTransaction();
            realm.close();
        }).start();
    }

    private void setOrderStates(List<OrderStateDTO> orderStateDTOS) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            orderStateDTOS.forEach(oerderState -> {

                OrderState orderSateRealm = realm.where(OrderState.class).equalTo("id", oerderState.getId()).findFirst();
                if (orderSateRealm == null) {
                    orderSateRealm = new OrderState();
                    orderSateRealm.setId(oerderState.getId());
                }
                orderSateRealm.setName(oerderState.getName());
                realm.copyToRealmOrUpdate(orderSateRealm);
            });
            realm.commitTransaction();
            realm.close();
        }).start();
    }

    public SharedPreferences getProfileSettings() {
        return profileSettings;
    }

    public int getCloudUserId() {
        return profileSettings.getInt("USER_ID", 1);
    }
    public void clearProfileSettings() {
        editor = profileSettings.edit();
        editor.clear();
    }
}
