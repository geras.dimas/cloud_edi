package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class OrderPositionPostDTO {
    @SerializedName("case")
    @Expose
    private int caseDb;

    @SerializedName("separate")
    @Expose
    private int separate;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("intialprice")
    @Expose
    private int initialPrice;

    @SerializedName("article_id")
    @Expose
    private int articleId;

    @SerializedName("order_id")
    @Expose
    private int orderID;

    @SerializedName("orderposition_uuid")
    @Expose
    private String orderPositionUUID;

    public OrderPositionPostDTO() {
    }

    public int getCaseDb() {
        return caseDb;
    }

    public void setCaseDb(int caseDb) {
        this.caseDb = caseDb;
    }

    public int getSeparate() {
        return separate;
    }

    public void setSeparate(int separate) {
        this.separate = separate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(int initialPrice) {
        this.initialPrice = initialPrice;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getOrderPositionUUID() {
        return orderPositionUUID;
    }

    public void setOrderPositionUUID(String orderPositionUUID) {
        this.orderPositionUUID = orderPositionUUID;
    }
}
