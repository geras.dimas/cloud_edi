package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteOrderPositionDTO {
    @SerializedName("delete")
    @Expose
    private int delete;

    @SerializedName("id")
    @Expose
    private int id;
    public DeleteOrderPositionDTO() {
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
