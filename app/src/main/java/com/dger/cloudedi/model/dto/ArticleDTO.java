package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.SerializedName;

public class ArticleDTO {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("itemcode")
    private String itemCode;

    @SerializedName("jan")
    private String jan;

    @SerializedName("standart")
    private String standart;

    @SerializedName("price")
    private int price;

    @SerializedName("remaining")
    private int remaining;

    @SerializedName("qtyperunit")
    private int qtyPerUnit;

    @SerializedName("supplier_id")
    private int supplierId;

    @SerializedName("Shop")
    private ShopDTO shop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getStandart() {
        return standart;
    }

    public void setStandart(String standart) {
        this.standart = standart;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public int getQtyPerUnit() {
        return qtyPerUnit;
    }

    public void setQtyPerUnit(int qtyPerUnit) {
        this.qtyPerUnit = qtyPerUnit;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public ShopDTO getShop() {
        return shop;
    }

    public void setShop(ShopDTO shop) {
        this.shop = shop;
    }
}
