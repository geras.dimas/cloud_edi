package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class OrderPositionGetDTO {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("case")
    private int dbCase;

    @SerializedName("intialprice")
    @Expose
    private int initialPrice;

    @SerializedName("separate")
    @Expose
    private int separate;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("Article")
    private ArticleDTO article;

    @SerializedName("Order")
    private OrderDTO order;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDbCase() {
        return dbCase;
    }

    public void setDbCase(int dbCase) {
        this.dbCase = dbCase;
    }

    public int getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(int initialPrice) {
        this.initialPrice = initialPrice;
    }

    public int getSeparate() {
        return separate;
    }

    public void setSeparate(int separate) {
        this.separate = separate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArticleDTO getArticle() {
        return article;
    }

    public void setArticle(ArticleDTO article) {
        this.article = article;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }
}
