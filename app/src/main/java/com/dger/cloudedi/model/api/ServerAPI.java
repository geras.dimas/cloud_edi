package com.dger.cloudedi.model.api;

import com.dger.cloudedi.model.dto.CategoryDTO;
import com.dger.cloudedi.model.dto.CreateDTO;
import com.dger.cloudedi.model.dto.CreatePositionDTO;
import com.dger.cloudedi.model.dto.DeleteOrderPositionDTO;
import com.dger.cloudedi.model.dto.DeliveryDTO;
import com.dger.cloudedi.model.dto.LoginDTO;
import com.dger.cloudedi.model.dto.OrderPositionGetDTO;
import com.dger.cloudedi.model.dto.OrderPositionPostDTO;
import com.dger.cloudedi.model.dto.OrderPostDTO;
import com.dger.cloudedi.model.dto.OrderSearchDTO;
import com.dger.cloudedi.model.dto.OrderSearchForCustomerDTO;
import com.dger.cloudedi.model.dto.OrderStateDTO;
import com.dger.cloudedi.model.dto.SlipTypeDTO;
import com.dger.cloudedi.model.dto.UserDTO;
import com.dger.cloudedi.model.dto.ValidateDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ServerAPI {
    @POST("/users/login")
    Observable<UserDTO> login(@Body LoginDTO login);

    @GET("/categories")
    Observable<List<CategoryDTO>> getCategories();

    @GET("/slip-type")
    Observable<List<SlipTypeDTO>> getSlipTypes();

    @GET("/deliveries")
    Observable<List<DeliveryDTO>> getDeliveries();

    @GET("/order-states")
    Observable<List<OrderStateDTO>> getOrderSates();

    @DELETE("/order-positions/{orderPosition}")
    Observable<DeleteOrderPositionDTO> deleteOrderPosition(@Path("orderPosition") int orderPosition);

    @POST("/orders")
    Observable<CreateDTO> createOrder(@Body OrderPostDTO order);


    @POST("/order-positions")
    Observable<CreatePositionDTO> createOrderPosition(@Body OrderPositionPostDTO position);

    @POST("/order-positions/search")
    Observable<List<OrderPositionGetDTO>> getOrders(@Body OrderSearchDTO orderSearchDTO);

    @GET("/orders/validate/{order}")
    Observable<ValidateDTO> validateOrder(@Path("order") int order);

    @POST("/order-positions/search-for-customer")
    Observable<List<OrderPositionGetDTO>> getOrdersForCustomer(@Body OrderSearchForCustomerDTO orderSearchDTO);

}
