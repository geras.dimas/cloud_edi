package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class OrderDTO {
    @SerializedName("id")
    private Integer id;

    @SerializedName("orderDare")
    private Date orderDate;

    @SerializedName("deliveryDate")
    private Date deliveryDate;

    @SerializedName("createdate")
    private Date createDate;

    @SerializedName("orderState_id")
    private int orderStateId;

    @SerializedName("orderType_id")
    private int orderTypeId;

    @SerializedName("category_id")
    private int categoryId;

    @SerializedName("delivery_id")
    private int deliveryId;

    @SerializedName("slipType_id")
    private int slipTypeId;

    @SerializedName("Shop")
    public ShopDTO shop;

    @SerializedName("user")
    public UserDTO user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getOrderStateId() {
        return orderStateId;
    }

    public void setOrderStateId(int orderStateId) {
        this.orderStateId = orderStateId;
    }

    public int getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(int orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getSlipTypeId() {
        return slipTypeId;
    }

    public void setSlipTypeId(int slipTypeId) {
        this.slipTypeId = slipTypeId;
    }

    public ShopDTO getShop() {
        return shop;
    }

    public void setShop(ShopDTO shop) {
        this.shop = shop;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
