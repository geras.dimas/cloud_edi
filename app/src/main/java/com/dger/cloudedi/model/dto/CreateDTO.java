package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateDTO {
    @SerializedName("id")
    @Expose
    private int id;

    public CreateDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
