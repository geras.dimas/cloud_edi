package com.dger.cloudedi.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class OrderPostDTO {
    @SerializedName("orderDare")
    @Expose
    private Date orderDate;

    @SerializedName("deliveryDate")
    @Expose
    private Date deliveryDate;

    @SerializedName("createdate")
    @Expose
    private Date createDate;

    @SerializedName("orderState_id")
    @Expose
    private int orderStateId;

    @SerializedName("category_id")
    @Expose
    private int categoryId;

    @SerializedName("delivery_id")
    @Expose
    private int deliveryId;

    @SerializedName("slipType_id")
    @Expose
    private int slipTypeId;

    @SerializedName("creator_id")
    @Expose
    private int creatorId;

    @SerializedName("shop_id")
    @Expose
    private int shopId;

    public OrderPostDTO() {
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getOrderStateId() {
        return orderStateId;
    }

    public void setOrderStateId(int orderStateId) {
        this.orderStateId = orderStateId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getSlipTypeId() {
        return slipTypeId;
    }

    public void setSlipTypeId(int slipTypeId) {
        this.slipTypeId = slipTypeId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
