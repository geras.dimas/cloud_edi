package com.dger.cloudedi.model;

import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.model.api.ServerAPI;
import com.dger.cloudedi.model.dto.CategoryDTO;
import com.dger.cloudedi.model.dto.CreateDTO;
import com.dger.cloudedi.model.dto.CreatePositionDTO;
import com.dger.cloudedi.model.dto.DeleteOrderPositionDTO;
import com.dger.cloudedi.model.dto.DeliveryDTO;
import com.dger.cloudedi.model.dto.LoginDTO;
import com.dger.cloudedi.model.dto.OrderPositionGetDTO;
import com.dger.cloudedi.model.dto.OrderPositionPostDTO;
import com.dger.cloudedi.model.dto.OrderPostDTO;
import com.dger.cloudedi.model.dto.OrderSearchDTO;
import com.dger.cloudedi.model.dto.OrderSearchForCustomerDTO;
import com.dger.cloudedi.model.dto.OrderStateDTO;
import com.dger.cloudedi.model.dto.SlipTypeDTO;
import com.dger.cloudedi.model.dto.UserDTO;
import com.dger.cloudedi.model.dto.ValidateDTO;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIService {
    @Inject
    public ServerAPI serverAPI;
    private Retrofit retrofit;
    private static APIService instance = null;

    public APIService(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:4000/") //base url
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()) //converter JSON to object
                .build();
        serverAPI = retrofit.create(ServerAPI.class);
    }

    public Observable<UserDTO> login(String login, String password){
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setLogin(login);
        loginDTO.setPassword(password);
        return serverAPI.login(loginDTO).subscribeOn(Schedulers.newThread());

    }

    public Observable<List<CategoryDTO>> getCategories(){
        return serverAPI.getCategories().subscribeOn(Schedulers.newThread());

    }

    public Observable<List<SlipTypeDTO>> getSlipTypes(){
        return serverAPI.getSlipTypes().subscribeOn(Schedulers.newThread());

    }

    public Observable<List<DeliveryDTO>> getDeliveries(){
        return serverAPI.getDeliveries().subscribeOn(Schedulers.newThread());

    }

    public Observable<List<OrderStateDTO>> getOrderStates(){
        return serverAPI.getOrderSates().subscribeOn(Schedulers.newThread());

    }

    public Observable<DeleteOrderPositionDTO> deleteOrderPosition(int orderPosition){
        return serverAPI.deleteOrderPosition(orderPosition).subscribeOn(Schedulers.newThread());

    }

    public Observable<CreateDTO> createOrder(Order order){
        OrderPostDTO postDTO = new OrderPostDTO();
        postDTO.setCategoryId(order.getCategory().getId());
        postDTO.setCreatorId(order.getCreator().getId());
        postDTO.setDeliveryDate(order.getDeliveryDate());
        postDTO.setOrderDate(order.getOrderDate());
        postDTO.setCreateDate(order.getCreateDate());
        postDTO.setDeliveryId(order.getDelivery().getId());
        postDTO.setShopId(order.getShop().getId());
        postDTO.setSlipTypeId(order.getSlipType().getId());
        postDTO.setOrderStateId(order.getOrderState().getId());
        return serverAPI.createOrder(postDTO).subscribeOn(Schedulers.newThread());

    }

    public Observable<CreatePositionDTO> createOrderPosition(OrderPosition orderPosition){
        OrderPositionPostDTO orderPositionPostDTO= new OrderPositionPostDTO();
        orderPositionPostDTO.setArticleId(orderPosition.getArticle().getId());
        orderPositionPostDTO.setCaseDb(orderPosition.getDbCase());
        orderPositionPostDTO.setOrderID(orderPosition.getOrder().getDbId());
        orderPositionPostDTO.setInitialPrice(orderPosition.getInitialPrice());
        orderPositionPostDTO.setOrderPositionUUID(orderPosition.getId());
        orderPositionPostDTO.setSeparate(orderPosition.getSeparate());
        orderPositionPostDTO.setTotal(orderPosition.getTotal());
        return serverAPI.createOrderPosition(orderPositionPostDTO).subscribeOn(Schedulers.newThread());

    }

    public Observable<List<OrderPositionGetDTO>> getOrders(int supplierId){
        OrderSearchDTO orderSearchDTO = new OrderSearchDTO();
        orderSearchDTO.setSupplierId(supplierId);
        return serverAPI.getOrders(orderSearchDTO).subscribeOn(Schedulers.newThread());

    }

    public Observable<List<OrderPositionGetDTO>> getOrdersForCustomer(int shopId){
        OrderSearchForCustomerDTO orderSearchDTO = new OrderSearchForCustomerDTO();
        orderSearchDTO.setShopId(shopId);
        return serverAPI.getOrdersForCustomer(orderSearchDTO).subscribeOn(Schedulers.newThread());

    }
    public Observable<ValidateDTO> validateOrder(int order){
        return serverAPI.validateOrder(order).subscribeOn(Schedulers.newThread());

    }

    public static APIService getInstance() {
        if(instance == null){
            instance = new APIService();
        }
        return instance;
    }
}
