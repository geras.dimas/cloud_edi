package com.dger.cloudedi.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okhttp3.logging.HttpLoggingInterceptor;

public class WebSocketService {

    private static WebSocketService instance = null;
    private WebSocket ws;
    private RealmService realmService;
    public WebSocketService() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("ws://10.0.2.2:8080").build();
        WebSocketCustomListener listener = new WebSocketCustomListener();
        realmService = new RealmService();
        ws = client.newWebSocket(request, listener);

    }

    class WebSocketCustomListener extends WebSocketListener {

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            webSocket.send("{\"type\":\"test\"}");

        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            try {
                JSONObject wsObj = new JSONObject(text);
                String type = wsObj.getString("type");
                switch (type){
                    case "findArticle":
                        JSONArray articles = wsObj.getJSONArray("data");
                        realmService.saveArticle(articles);
                    default:
                       System.out.println(text);
                }
            } catch ( Throwable t){
                System.err.println("Could not parseJSON:");
            }
        }
    }


    public void sendMessage(String message) {
        ws.send(message);
    }
    public static WebSocketService getInstance() {
        if(instance == null){
            instance = new WebSocketService();
        }
        return instance;
    }
}

