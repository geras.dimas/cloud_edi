package com.dger.cloudedi.model;

import com.dger.cloudedi.bean.Article;
import com.dger.cloudedi.bean.Shop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;

public class RealmService {

    private Realm realm;

    RealmService(){

    }

    public void saveArticle(JSONArray articles){
        try {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            for (int i = 0; i < articles.length(); i++) {
                JSONObject article = articles.getJSONObject(i);
                Article articleRealm = realm.where(Article.class).equalTo("id", article.getInt("id")).findFirst();;

                if (articleRealm == null){
                    articleRealm = new Article();
                    articleRealm.setId(article.getInt("id"));
                }
                articleRealm.setItemCode(article.getString("itemcode"));
                articleRealm.setJan(article.getString("jan"));
                articleRealm.setStandart(article.getString("standart"));
                articleRealm.setPrice(article.getInt("price"));
                articleRealm.setQtyPerUnit(article.getInt("qtyperunit"));
                articleRealm.setRemaining(article.getInt("remaining"));
                articleRealm.setName(article.getString("name"));

                JSONObject shop = article.getJSONObject("Shop");

                Shop shopRealm = realm.where(Shop.class).equalTo("id", shop.getInt("id")).findFirst();
                if (shopRealm == null){
                    shopRealm = new Shop();
                    shopRealm.setId(shop.getInt("id"));
                }
                shopRealm.setName(shop.getString("name"));
                realm.copyToRealmOrUpdate(shopRealm);

                articleRealm.setSupplier(shopRealm);

                realm.copyToRealmOrUpdate(articleRealm);
            }
            realm.commitTransaction();
            realm.close();
        }catch (JSONException e) {
            realm.close();
            e.printStackTrace();
        }
    }
}
