package com.dger.cloudedi.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;

import java.text.SimpleDateFormat;
import java.util.List;

public class OrderEntityAdapter extends ArrayAdapter<Order> {
    private LayoutInflater inflater;
    private int layout;
    private List<Order> orders;
    SimpleDateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy HH:mm:ss" );

    public OrderEntityAdapter(Context context, int layout, List<Order> orders) {
        super(context, layout, orders);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.orders = orders;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view=inflater.inflate(this.layout,parent,false);

        TextView header=(TextView) view.findViewById(R.id.order_name);
        TextView orderItem = (TextView) view.findViewById(R.id.order_item);

        Order order = orders.get(position);

        header.setText(dateFormat.format(order.getCreateDate()));
        List<OrderPosition> orderPositions = order.getOrderPositions();
        int item = orderPositions.size();
        int price =orderPositions.stream().mapToInt(orderPosition -> orderPosition.getTotal() * (orderPosition.getInitialPrice()>0 ? orderPosition.getInitialPrice() : orderPosition.getArticle().getPrice())).sum();
        int qty = orderPositions.stream().mapToInt(orderPosition -> orderPosition.getTotal()).sum();
        orderItem.setText("(Item : " + item + ", Qty : " + qty + ", JPY : " + price + ")");

        return view;
    }
}
