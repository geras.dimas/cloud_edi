package com.dger.cloudedi.ui.sellers;

import android.os.Bundle;
import android.view.Menu;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.SellerPresenter;
import com.dger.cloudedi.view.SellerView;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class SellerActivity extends MvpAppCompatActivity implements SellerView {

    private AppBarConfiguration mAppBarConfiguration;
    @InjectPresenter
    SellerPresenter sellerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller);
        Toolbar toolbar = findViewById(R.id.toolbar_seller);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout_seller);
        NavigationView navigationView = findViewById(R.id.nav_view_seller);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_customer_home,  R.id.nav_profile)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_seller);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        sellerPresenter.addSelectAllInSpinner();
        sellerPresenter.getOrders();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_seller_drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_seller);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void getOrders(){
        sellerPresenter.getOrders();
    }
}
