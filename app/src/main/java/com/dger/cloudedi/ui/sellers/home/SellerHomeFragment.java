package com.dger.cloudedi.ui.sellers.home;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.SellerHomePresenter;
import com.dger.cloudedi.presenter.SellerPresenter;
import com.dger.cloudedi.ui.sellers.SellerActivity;
import com.dger.cloudedi.view.SellerHomeVew;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class SellerHomeFragment extends MvpAppCompatFragment implements SellerHomeVew {

    @InjectPresenter
    SellerHomePresenter sellerHomePresenter;
    TextView countOrder;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.seller_home_fragment, container, false);
        countOrder = root.findViewById(R.id.seller_home_count_order);

        countOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_nav_seller_home_to_nav_seller_order_list);
            }
        });

        SellerActivity sellerActivity = (SellerActivity)getActivity();
        sellerActivity.getOrders();
        sellerHomePresenter.setOrderCount();

        return root;
    }


    @Override
    public void setOrderCount(long lOrderCount) {
        countOrder.setText(Long.toString(lOrderCount));
    }

}