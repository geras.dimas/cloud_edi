package com.dger.cloudedi.ui.logout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.LogoutPresenter;
import com.dger.cloudedi.ui.customer.CustomerActivity;
import com.dger.cloudedi.ui.customer.purchordfunc.PurchaseOrderFunctionViewModel;
import com.dger.cloudedi.ui.login.LoginActivity;
import com.dger.cloudedi.view.LoginView;
import com.dger.cloudedi.view.LogoutView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class LogoutFragment extends MvpAppCompatFragment implements LogoutView {

    @InjectPresenter
    public LogoutPresenter logoutPresenter;
    private Button logoutButton;
    public TextView userName, email;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.profile_fragment, container, false);

        email = (TextView) root.findViewById(R.id.email);
        userName = (TextView) root.findViewById(R.id.user_name);

        logoutButton = (Button) root.findViewById(R.id.logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutPresenter.logout();
            }
        });

        prefs = getActivity().getSharedPreferences("mysettings", LoginActivity.MODE_PRIVATE);
        editor = prefs.edit();
        logoutPresenter.initParams();
        return root;
    }


    @Override
    public void setEmail(String sEmail) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                email.setText(sEmail);
            }
        });
    }

    @Override
    public void setUserName(String sUserName) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                userName.setText(sUserName);
            }
        });
    }

    @Override
    public void transitionLoginView() {

        Intent main = new Intent(getActivity(), LoginActivity.class);
        getActivity().startActivity(main);
    }

}
