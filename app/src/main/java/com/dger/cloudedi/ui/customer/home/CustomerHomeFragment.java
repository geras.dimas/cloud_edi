package com.dger.cloudedi.ui.customer.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.CustomerHomePresenter;
import com.dger.cloudedi.ui.customer.CustomerActivity;
import com.dger.cloudedi.view.CustomerHomeView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class CustomerHomeFragment extends MvpAppCompatFragment implements CustomerHomeView {

    @InjectPresenter
    CustomerHomePresenter customerHomePresenter;
    TextView countOrder;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.customer_fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_seller_home);

        countOrder = root.findViewById(R.id.customer_home_count_order);

        countOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_nav_customer_home_to_nav_customer_order_list);
            }
        });

        CustomerActivity customerActivity = (CustomerActivity)getActivity();
        customerActivity.getOrders();
        customerHomePresenter.setOrderCount();

        return root;
    }

    @Override
    public void setOrderCount(long lOrderCount) {
        countOrder.setText(Long.toString(lOrderCount));
    }
}