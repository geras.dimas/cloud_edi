package com.dger.cloudedi.ui.customer.edditposition;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.EditPositionPresenter;
import com.dger.cloudedi.ui.customer.purchordfunc.PurchaseOrderFunctionViewModel;
import com.dger.cloudedi.view.EditPositionView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class EditOrderPosition extends MvpAppCompatFragment implements EditPositionView {

    @InjectPresenter
    EditPositionPresenter editPositionPresenter;
    TextView positionName, itemCode, jan, standard, qtyPerUnit, supplier, primeCost;
    EditText total, operationCase, separator, initialPrice;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.customer_fragmet_order_position_layout, container, false);
        positionName = (TextView) root.findViewById(R.id.position_name);
        itemCode = (TextView) root.findViewById(R.id.position_item_code);
        jan = (TextView) root.findViewById(R.id.position_jan);
        standard = (TextView) root.findViewById(R.id.position_standart);
        qtyPerUnit = (TextView) root.findViewById(R.id.position_qty);
        supplier = (TextView) root.findViewById(R.id.position_supplier);
        primeCost = (TextView) root.findViewById(R.id.position_prime_cost);

        total = (EditText) root.findViewById(R.id.position_totsl);
        operationCase = (EditText) root.findViewById(R.id.position_case);
        separator = (EditText) root.findViewById(R.id.position_separate);
        initialPrice = (EditText) root.findViewById(R.id.position_init_price);

        total.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (total.getText().toString().length()>0) {
                    editPositionPresenter.updateTotal(Integer.parseInt(total.getText().toString()));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


            }
        });

        operationCase.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (operationCase.getText().toString().length()>0) {
                    editPositionPresenter.updateOperationCase(Integer.parseInt(operationCase.getText().toString()));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        separator.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (separator.getText().toString().length()>0) {
                    editPositionPresenter.updateSeparator(Integer.parseInt(separator.getText().toString()));
                }
        }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

        });

        initialPrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (initialPrice.getText().toString().length()>0) {
                    editPositionPresenter.updateInitialPrice(Integer.parseInt(initialPrice.getText().toString()));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });



        editPositionPresenter.setOrderPosition(getArguments());
        return root;
    }

    @Override
    public void setPositionName(String sPositionName) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                positionName.setText(sPositionName);
            }
        });
    }

    @Override
    public void setItemCode(String sItemCode) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                itemCode.setText(sItemCode);

            }
        });
    }

    @Override
    public void setJAN(String sJan) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                jan.setText(sJan);
            }
        });
    }

    @Override
    public void setStandard(String sStandart) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                standard.setText(sStandart);
            }
        });
    }

    @Override
    public void setQtyPerUnit(int sQtyPerUnit) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                qtyPerUnit.setText(Integer.toString(sQtyPerUnit));
            }
        });
    }

    @Override
    public void setSupplier(String sSupplier) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                supplier.setText(sSupplier);
            }
        });
    }

    @Override
    public void setPrimeCost(int sPrimeCost) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                primeCost.setText(Integer.toString(sPrimeCost));
            }
        });
    }

    @Override
    public void setTotal(int sTotal) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                total.setText(Integer.toString(sTotal));
            }
        });
    }

    @Override
    public void setOperationCase(int sOperationCase) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                operationCase.setText(Integer.toString(sOperationCase));
            }
        });
    }

    @Override
    public void setSeparator(int sSeparator) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                separator.setText(Integer.toString(sSeparator));
            }
        });
    }

    @Override
    public void setInitialPrice(int sInitialPrice) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initialPrice.setText(Integer.toString(sInitialPrice));
            }
        });
    }
}