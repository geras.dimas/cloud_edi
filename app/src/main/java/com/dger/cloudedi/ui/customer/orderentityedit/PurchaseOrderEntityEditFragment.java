package com.dger.cloudedi.ui.customer.orderentityedit;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.presenter.PurchaseOrderEntityEditPresenter;
import com.dger.cloudedi.ui.adapters.OrderPositionAdapter;
import com.dger.cloudedi.view.PurchaseOrderEntityEditView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class PurchaseOrderEntityEditFragment extends MvpAppCompatFragment implements PurchaseOrderEntityEditView {

    @InjectPresenter
    PurchaseOrderEntityEditPresenter entityEditPresenter;
    ImageButton orderDateButton, deliveryDateButton;
    EditText orderDate, deliveryDate;
    private int mOrderYear, mOrderMonth, mOrderDay,mDeliveryYear, mDeliveryMonth, mDeliveryDay;
    DatePickerDialog datePickerDialog;
    Spinner categorySpinner, deliverySpinner, slipTypeSpiner;
    View root;
    Calendar orderDateCalendar, deliveryDateCalendar;
    DatePickerDialog datePickerDialogOrder, datePickerDialogDelivery;
    SimpleDateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
    Button addPosition, confirmOrder;

    ListView orderPositionListView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.customer_fragment_add_order_entity    , container, false);

        categorySpinner = (Spinner) root.findViewById(R.id.category_spinner);
        slipTypeSpiner = (Spinner) root.findViewById(R.id.slip_type_spinner);
        deliverySpinner = (Spinner) root.findViewById(R.id.delivery_spinner);

        orderDateButton=(ImageButton) root.findViewById(R.id.order_date_button);
        orderDate = (EditText) root.findViewById(R.id.order_date);

        deliveryDateButton=(ImageButton) root.findViewById(R.id.delivery_date_button);
        deliveryDate = (EditText) root.findViewById(R.id.delivery_date);

        addPosition = (Button) root.findViewById(R.id.add_order_position);

        confirmOrder = (Button) root.findViewById(R.id.confirm_order);

        addPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle orderBundle = new Bundle();
                orderBundle.putString("orderUUID",  entityEditPresenter.getOrderUUID());
                setArguments(orderBundle);
                Navigation.findNavController(view).navigate(R.id.action_nav_purchase_order_entity_edit_to_nav_purchase_order_entity_add_position, orderBundle);
            }
        });

        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                entityEditPresenter.confirmOrder();
            }
        });

        orderPositionListView = (ListView) root.findViewById(R.id.order_position_list);

        entityEditPresenter.setOrder(getArguments());
        registerForContextMenu(orderPositionListView);
        return root;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.context_order_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_order:
                editOrder(info.position); // метод, выполняющий действие при редактировании пункта меню
                return true;
            case R.id.delete_order:
                deleteItem(info.position); //метод, выполняющий действие при удалении пункта меню
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void editOrder(int position){
        OrderPosition orderPosition = (OrderPosition) orderPositionListView.getItemAtPosition(position);
        Bundle orderBundle = new Bundle();
        orderBundle.putString("orderUUID",  entityEditPresenter.getOrderUUID());
        orderBundle.putString("orderPositionUUID", orderPosition.getId());
        Navigation.findNavController(root).navigate(R.id.action_nav_purchase_order_entity_edit_to_nav_purchase_order_entity_edit_position, orderBundle);
    }

    private void deleteItem(int position){
        OrderPosition orderPosition = (OrderPosition) orderPositionListView.getItemAtPosition(position);
        entityEditPresenter.deleteOrderPosition(orderPosition.getId());
    }

    public void setCategory(List<Category> categoryList, Category category) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(root.getContext(),
                        android.R.layout.simple_spinner_item, categoryList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                categorySpinner.setAdapter(adapter);
                categorySpinner.setSelection(categoryList.indexOf(category));

                categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        Category category = (Category) parentView.getItemAtPosition(position);
                        entityEditPresenter.updateCategory(category);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }});
    }

    public void setSlipType(List<SlipType> slipTypeList, SlipType slipType) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<SlipType> adapter = new ArrayAdapter<SlipType>(root.getContext(),
                        android.R.layout.simple_spinner_item, slipTypeList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                slipTypeSpiner.setAdapter(adapter);
                slipTypeSpiner.setSelection(slipTypeList.indexOf(slipType));

                slipTypeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        SlipType slipTypeSelect = (SlipType) parentView.getItemAtPosition(position);
                        entityEditPresenter.updateSlipType(slipTypeSelect);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });

            }});
    }

    public void setDelivery(List<Delivery> deliveryList, Delivery delivery) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<Delivery> adapter = new ArrayAdapter<Delivery>(root.getContext(),
                        android.R.layout.simple_spinner_item, deliveryList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                deliverySpinner.setAdapter(adapter);
                deliverySpinner.setSelection(deliveryList.indexOf(delivery));

                deliverySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        Delivery deliverySelect = (Delivery) parentView.getItemAtPosition(position);
                        entityEditPresenter.updateDelivery(deliverySelect);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }});
    }

    public void setOrderDate(Date sOrderDate){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                orderDate.setText(dateFormat.format(sOrderDate));
                orderDateCalendar = Calendar.getInstance();
                orderDateCalendar.setTime(sOrderDate);
                mOrderYear = orderDateCalendar.get(Calendar.YEAR);
                mOrderMonth = orderDateCalendar.get(Calendar.MONTH);
                mOrderDay = orderDateCalendar.get(Calendar.DAY_OF_MONTH);
                orderDate.setText(mOrderDay + "-" + (mOrderMonth + 1) + "-" + mOrderYear);
                datePickerDialogOrder = new DatePickerDialog(root.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                orderDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                orderDateCalendar.set(year, monthOfYear, dayOfMonth);
                                entityEditPresenter.updateOrderDate(orderDateCalendar.getTime());
                            }
                        }, mOrderYear, mOrderMonth, mOrderDay);

                orderDateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialogOrder.show();
                    }
                });
            }
        });
    }
    public void setDeliveryDate(Date sDeliveryDate){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                deliveryDate.setText(dateFormat.format(sDeliveryDate));

                deliveryDateCalendar = Calendar.getInstance();
                deliveryDateCalendar.setTime(sDeliveryDate);
                mDeliveryYear = deliveryDateCalendar.get(Calendar.YEAR);
                mDeliveryMonth = deliveryDateCalendar.get(Calendar.MONTH);
                mDeliveryDay = deliveryDateCalendar.get(Calendar.DAY_OF_MONTH);
                deliveryDate.setText(mDeliveryDay + "-" + (mDeliveryMonth + 1) + "-" + mDeliveryYear);
                datePickerDialogDelivery = new DatePickerDialog(root.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                deliveryDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                deliveryDateCalendar.set(year, monthOfYear, dayOfMonth);
                                entityEditPresenter.updateDeliveryDate(deliveryDateCalendar.getTime());
                            }
                        }, mDeliveryYear, mDeliveryMonth, mDeliveryDay);

                deliveryDateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialogDelivery.show();
                    }
                });
            }
        });
    }

    @Override
    public void setOrderPositionList(List<OrderPosition> orderPositionList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                OrderPositionAdapter orderPositionAdapter = new OrderPositionAdapter(getContext(), R.layout.customer_order_position_item, orderPositionList);
                orderPositionListView.setAdapter(orderPositionAdapter);
                orderPositionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        OrderPosition orderPosition = (OrderPosition) orderPositionListView.getItemAtPosition(position);

                        Bundle orderBundle = new Bundle();
                        orderBundle.putString("orderUUID",  entityEditPresenter.getOrderUUID());
                        setArguments(orderBundle);
                        orderBundle.putString("orderPositionUUID", orderPosition.getId());
                        Navigation.findNavController(root).navigate(R.id.action_nav_purchase_order_entity_edit_to_nav_purchase_order_entity_edit_position, orderBundle);



                    }
                });
            }
        });
    }

    @Override
    public void showToast(String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(root.getContext(),
                        text, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
            }
        });
    }

    @Override
    public void blockButton() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addPosition.setVisibility(View.GONE);
                confirmOrder.setVisibility(View.GONE);
            }
        });
    }


}