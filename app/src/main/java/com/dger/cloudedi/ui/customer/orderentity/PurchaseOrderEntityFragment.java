package com.dger.cloudedi.ui.customer.orderentity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.presenter.PurchaseOrderEntityPresenter;
import com.dger.cloudedi.ui.adapters.OrderEntityAdapter;
import com.dger.cloudedi.view.PurchaseOrderEntity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class PurchaseOrderEntityFragment extends MvpAppCompatFragment implements PurchaseOrderEntity {

    @InjectPresenter
    PurchaseOrderEntityPresenter entityPresenter;
    ListView orderListView;
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.customer_fragment_purchase_order_enty, container, false);
        Button button = (Button) root.findViewById(R.id.order_entity_button_add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_nav_purchase_order_entity_to_nav_purchase_order_entity_edit);
            }
        });
        orderListView = (ListView) root.findViewById(R.id.order_list);
        entityPresenter.setOrders();
        registerForContextMenu(orderListView);
        return root;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.context_order_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_order:
                editOrder(info.position); // метод, выполняющий действие при редактировании пункта меню
                return true;
            case R.id.delete_order:
                deleteItem(info.position); //метод, выполняющий действие при удалении пункта меню
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void editOrder(int position){
        Order order = (Order) orderListView.getItemAtPosition(position);
        Bundle orderBundle = new Bundle();
        orderBundle.putString("orderUUID", order.getId());
        Navigation.findNavController(root).navigate(R.id.action_nav_purchase_order_entity_to_nav_purchase_order_entity_edit, orderBundle);
    }

    private void deleteItem(int position){
        Order order = (Order) orderListView.getItemAtPosition(position);
        entityPresenter.deleteOrderPosition(order.getId());
    }
    @Override
    public void setOrders(List<Order> orderList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                OrderEntityAdapter orderEntityAdapter = new OrderEntityAdapter(getContext(), R.layout.customer_fragment_item_order_entity, orderList);
                orderListView.setAdapter(orderEntityAdapter);
                orderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        editOrder(position);
                    }
                });
            }
        });
    }
}