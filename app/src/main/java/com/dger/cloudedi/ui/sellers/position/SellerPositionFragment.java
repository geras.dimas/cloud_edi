package com.dger.cloudedi.ui.sellers.position;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.SellerPositionPresenter;
import com.dger.cloudedi.view.SellerPositionView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class SellerPositionFragment extends MvpAppCompatFragment implements SellerPositionView {

    @InjectPresenter
    SellerPositionPresenter sellerPositionPresenter;
    TextView sPositionName, sPositionItemCode, sPositionJan, sPositionStandard, sPositionQty,
            sPositionSupplier, sPositionCase, sPositionSeparate, sPositionTotal, sPositionInitialPrice, sPositionPrimeCost;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.seller_order_position, container, false);
        sPositionName = (TextView)root.findViewById(R.id.seller_position_name);
        sPositionItemCode = (TextView)root.findViewById(R.id.seller_position_item_code);
        sPositionJan = (TextView)root.findViewById(R.id.seller_position_jan);
        sPositionStandard = (TextView)root.findViewById(R.id.seller_position_standart);
        sPositionQty = (TextView)root.findViewById(R.id.seller_position_qty);
        sPositionSupplier = (TextView)root.findViewById(R.id.seller_position_supplier);
        sPositionCase = (TextView)root.findViewById(R.id.seller_position_case);
        sPositionSeparate = (TextView)root.findViewById(R.id.seller_position_separate);
        sPositionTotal = (TextView)root.findViewById(R.id.seller_position_total);
        sPositionInitialPrice = (TextView)root.findViewById(R.id.seller_position_initial_price);
        sPositionPrimeCost = (TextView)root.findViewById(R.id.seller_position_prime_cost);

        sellerPositionPresenter.setPosition(getArguments());
        return root;
    }

    @Override
    public void setName(String name) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionName.setText(name);
            }
        });
    }

    @Override
    public void setItemCode(String itemCode) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionItemCode.setText(itemCode);
            }
        });
    }

    @Override
    public void setJan(String jan) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionJan.setText(jan);
            }
        });
    }

    @Override
    public void setStandard(String standard) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionStandard.setText(standard);
            }
        });
    }

    @Override
    public void setQty(String qty) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionQty.setText(qty);
            }
        });
    }

    @Override
    public void setSupplier(String supplier) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionSupplier.setText(supplier);
            }
        });
    }

    @Override
    public void setPrimeCost(String primeCost) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionPrimeCost.setText(primeCost);
            }
        });
    }

    @Override
    public void setCase(String dbCase) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionCase.setText(dbCase);
            }
        });
    }

    @Override
    public void setSeparate(String separate) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionSeparate.setText(separate);
            }
        });
    }

    @Override
    public void setTotal(String total) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionTotal.setText(total);
            }
        });
    }

    @Override
    public void setInitialPrice(String initialPrice) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPositionInitialPrice.setText(initialPrice);
            }
        });
    }
}
