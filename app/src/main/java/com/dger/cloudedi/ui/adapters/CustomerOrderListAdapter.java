package com.dger.cloudedi.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;

import java.text.SimpleDateFormat;
import java.util.List;

public class CustomerOrderListAdapter extends ArrayAdapter<Order> {
    private LayoutInflater inflater;
    private int layout;
    private List<Order> orders;
    SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy/MM/dd" );

    public CustomerOrderListAdapter(Context context, int layout, List<Order> orders) {
        super(context, layout, orders);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.orders = orders;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view=inflater.inflate(this.layout,parent,false);

        TextView orderDate=(TextView) view.findViewById(R.id.customer_order_item_date);
        TextView shopName = (TextView) view.findViewById(R.id.customer_order_item_shop);
        TextView category = (TextView) view.findViewById(R.id.customer_order_item_category);
        TextView delivery = (TextView) view.findViewById(R.id.customer_order_item_delivery);
        TextView slipType = (TextView) view.findViewById(R.id.customer_order_item_slip_type);
        TextView deliveryDate = (TextView) view.findViewById(R.id.customer_order_item_delivery_date);
        TextView totalPrice = (TextView) view.findViewById(R.id.customer_order_item_total_price);

        Order order = orders.get(position);

        orderDate.setText(dateFormat.format(order.getOrderDate()));
        deliveryDate.setText("Delivery date - "+dateFormat.format(order.getDeliveryDate()));

        category.setText("Category - " + order.getCategory().getName());
        delivery.setText("Delivery - " + order.getDelivery().getName());
        slipType.setText("Category - " + order.getSlipType().getName());
        List<OrderPosition> orderPositions = order.getOrderPositions();
        shopName.setText(orderPositions.size()>0?orderPositions.get(0).getArticle().getSupplier().getName():"");
        int price = orderPositions.stream().mapToInt(orderPosition -> orderPosition.getTotal() * (orderPosition.getInitialPrice()>0 ? orderPosition.getInitialPrice() : orderPosition.getArticle().getPrice())).sum();
        totalPrice.setText(Integer.toString(price));

        return view;
    }
}
