package com.dger.cloudedi.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Article;
import com.dger.cloudedi.bean.Order;

import java.text.SimpleDateFormat;
import java.util.List;

public class ArticleAdapter extends ArrayAdapter<Article> {
    private LayoutInflater inflater;
    private int layout;
    private List<Article> articles;

    public ArticleAdapter(Context context, int layout, List<Article> articles) {
        super(context, layout, articles);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.articles = articles;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view=inflater.inflate(this.layout,parent,false);

        TextView header=(TextView) view.findViewById(R.id.article_name);
        TextView articleItemCode = (TextView) view.findViewById(R.id.article_itemcode);
        TextView articleShop = (TextView) view.findViewById(R.id.article_shop);

        Article article = articles.get(position);

        header.setText(article.getName());
        articleItemCode.setText(article.getItemCode());
        articleShop.setText(article.getSupplier().getName());

        return view;
    }
}
