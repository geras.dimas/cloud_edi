package com.dger.cloudedi.ui.login;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.dger.cloudedi.App;
import com.dger.cloudedi.R;
import com.dger.cloudedi.presenter.LoginPresenter;
import com.dger.cloudedi.ui.customer.CustomerActivity;
import com.dger.cloudedi.ui.sellers.SellerActivity;
import com.dger.cloudedi.view.LoginView;

public class LoginActivity  extends MvpAppCompatActivity implements LoginView {

    @InjectPresenter
    LoginPresenter loginPresenter;
    private EditText mLogin;
    private EditText mPassword;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLogin = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);

        prefs = App.get().getProfileSettings();
        editor = prefs.edit();
        boolean isAuth = prefs.getBoolean("USER_AUTH", false);
        int userRole = prefs.getInt("USER_ROLE_ID", 1);
        if(isAuth){
            if(userRole == 1){
                Intent customerActivity = new Intent(this, CustomerActivity.class);
                startActivity(customerActivity);
                finish();
            }
            if(userRole == 2){
                Intent sellerActivity = new Intent(this, SellerActivity.class);
                startActivity(sellerActivity);
                finish();
            }
        }
    }


    @Override
    public void setError(Integer idView, String errorMsg) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (idView) {
                    case R.id.login:
                        mLogin.setError(errorMsg);
                        break;
                }
            }
        });
    }

    @Override
    public void successVerify(int roleId) {
        if(roleId == 1){
            Intent customerActivity = new Intent(this, CustomerActivity.class);
            startActivity(customerActivity);
            finish();
        }
        if(roleId == 2){
            Intent sellerActivity = new Intent(this, SellerActivity.class);
            startActivity(sellerActivity);
            finish();
        }
    }

    @Override
    public void savePref(String name, int value){
        editor.putInt(name,value);
        editor.commit();
    }

    @Override
    public void savePref(String name, String value){
        editor.putString(name,value);
        editor.commit();
    }

    @Override
    public void savePref(String name, boolean value){
        editor.putBoolean(name,value);
        editor.commit();
    }

    public void clickLoginButton(View view) {

        String login = mLogin.getText().toString();
        String password = mPassword.getText().toString();
        if (TextUtils.isEmpty(login)) {
            mLogin.setError("Login is not be empty");
            return;
        }
        loginPresenter.loggin(login, password);
    }
}
