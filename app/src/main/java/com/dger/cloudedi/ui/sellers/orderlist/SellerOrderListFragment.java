package com.dger.cloudedi.ui.sellers.orderlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.presenter.SellerOrderListPresenter;
import com.dger.cloudedi.ui.adapters.OrderEntityAdapter;
import com.dger.cloudedi.ui.adapters.SellerOrderListAdapter;
import com.dger.cloudedi.view.SellerOrderListView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import moxy.InjectViewState;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class SellerOrderListFragment extends MvpAppCompatFragment implements SellerOrderListView {

    @InjectPresenter
    SellerOrderListPresenter sellerOrderListPresenter;
    Spinner categorySpinner, deliverySpinner, slipTypeSpinner, orderStateSpinner;
    EditText shopName;
    Button searchButton;
    ListView orderListView;
    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.seller_fragment_order_list, container, false);

        categorySpinner = (Spinner) root.findViewById(R.id.seller_spinner_category);
        slipTypeSpinner = (Spinner) root.findViewById(R.id.seller_spinner_slip_type);
        deliverySpinner = (Spinner) root.findViewById(R.id.seller_spinner_delivery);
        orderStateSpinner = (Spinner) root.findViewById(R.id.seller_spinner_order_state);
        shopName = (EditText) root.findViewById(R.id.seller_orders_shop);
        searchButton = (Button) root.findViewById(R.id.seller_order_search);
        orderListView = (ListView) root.findViewById(R.id.seller_orders_list);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sellerOrderListPresenter.setOrders();
            }
        });
        sellerOrderListPresenter.init();
        return root;
    }

    public void setCategory(List<Category> categoryList, Category category) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(root.getContext(),
                        android.R.layout.simple_spinner_item, categoryList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                categorySpinner.setAdapter(adapter);
                categorySpinner.setSelection(categoryList.indexOf(category));

                categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        Category category = (Category) parentView.getItemAtPosition(position);
                        sellerOrderListPresenter.updateCategory(category);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }});
    }

    public void setSlipType(List<SlipType> slipTypeList, SlipType slipType) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<SlipType> adapter = new ArrayAdapter<SlipType>(root.getContext(),
                        android.R.layout.simple_spinner_item, slipTypeList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                slipTypeSpinner.setAdapter(adapter);
                slipTypeSpinner.setSelection(slipTypeList.indexOf(slipType));

                slipTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        SlipType slipTypeSelect = (SlipType) parentView.getItemAtPosition(position);
                        sellerOrderListPresenter.updateSlipType(slipTypeSelect);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });

            }});
    }

    public void setDelivery(List<Delivery> deliveryList, Delivery delivery) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<Delivery> adapter = new ArrayAdapter<Delivery>(root.getContext(),
                        android.R.layout.simple_spinner_item, deliveryList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                deliverySpinner.setAdapter(adapter);
                deliverySpinner.setSelection(deliveryList.indexOf(delivery));

                deliverySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        Delivery deliverySelect = (Delivery) parentView.getItemAtPosition(position);
                        sellerOrderListPresenter.updateDelivery(deliverySelect);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }});
    }

    @Override
    public void setOrderState(List<OrderState> orderStateList, OrderState orderState) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<OrderState> adapter = new ArrayAdapter<OrderState>(root.getContext(),
                        android.R.layout.simple_spinner_item, orderStateList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                orderStateSpinner.setAdapter(adapter);
                orderStateSpinner.setSelection(orderStateList.indexOf(orderState));

                orderStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        OrderState orderStateL = (OrderState) parentView.getItemAtPosition(position);
                        sellerOrderListPresenter.updateOrderState(orderStateL);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }});
    }

    @Override
    public void setOrderList(List<Order> orderList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SellerOrderListAdapter sellerOrderListAdapter = new SellerOrderListAdapter(getContext(), R.layout.seller_orders_item, orderList);
                orderListView.setAdapter(sellerOrderListAdapter);
                orderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        openOrderFragment(position);
                    }
                });
            }
        });
    }

    private void openOrderFragment(int position){
        Order order = (Order) orderListView.getItemAtPosition(position);
        Bundle orderBundle = new Bundle();
        orderBundle.putString("orderUUID", order.getId());
        Navigation.findNavController(root).navigate(R.id.action_nav_seller_order_list_to_nav_seller_order, orderBundle);
    }
}
