package com.dger.cloudedi.ui.customer.addorderposition;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Article;
import com.dger.cloudedi.presenter.AddPositionPresenter;
import com.dger.cloudedi.ui.adapters.ArticleAdapter;
import com.dger.cloudedi.ui.adapters.OrderEntityAdapter;
import com.dger.cloudedi.ui.customer.purchordfunc.PurchaseOrderFunctionViewModel;
import com.dger.cloudedi.view.AddPositionView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class AddOrderPositionFragment extends MvpAppCompatFragment implements AddPositionView {

    @InjectPresenter
    AddPositionPresenter addPositionPresenter;
    ImageButton searchButton, scanBarcode;
    View root;
    ListView articleListView;
    private EditText searchString;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.customer_fragment_add_order_position, container, false);
        getFragmentManager();
        searchString = (EditText)root.findViewById(R.id.search_order_position);
        articleListView = (ListView) root.findViewById(R.id.article_list);
        searchButton = (ImageButton) root.findViewById(R.id.search_position);
        scanBarcode = (ImageButton) root.findViewById(R.id.scan_barcode);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               addPositionPresenter.searchArticle(searchString.getText().toString());
            }
        });

        scanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanFromFragment();
            }
        });
        addPositionPresenter.setOrder(getArguments());
        return root;
    }


    @Override
    public void setArticleList(List<Article> articleList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArticleAdapter articleAdapter = new ArticleAdapter(getContext(), R.layout.customer_article_item, articleList);
                articleListView.setAdapter(articleAdapter);
                articleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        Article article = (Article) articleListView.getItemAtPosition(position);

                        addPositionPresenter.AddArticleInOrder(article);
                    }
                });
            }
        });
    }

    @Override
    public void showToast(String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(root.getContext(),
                        text, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
            }
        });
    }
    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this).setCameraId(0).initiateScan();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                showToast("failed to recognize");
            } else {
                setSearchString(result.getContents());
                addPositionPresenter.searchArticle(result.getContents());
            }

        }
    }

    public void setSearchString(String string){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                searchString.setText(string);
            }
        });
    }
}