package com.dger.cloudedi.ui.customer.purchordfunc;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PurchaseOrderFunctionViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PurchaseOrderFunctionViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is test fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }


}