package com.dger.cloudedi.ui.customer.purchordfunc;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dger.cloudedi.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

public class PurchaseOrderFunctionFragment extends Fragment {

    private PurchaseOrderFunctionViewModel orderFunctionViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        orderFunctionViewModel =
                ViewModelProviders.of(this).get(PurchaseOrderFunctionViewModel.class);
        View root = inflater.inflate(R.layout.customer_fragment_purchase_order_function, container, false);
        Button button = (Button) root.findViewById(R.id.order_entity_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_nav_purchase_order_function_to_nav_purchase_order_entity);
            }
        });

        return root;
    }
}