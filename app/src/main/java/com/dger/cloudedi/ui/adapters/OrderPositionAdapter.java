package com.dger.cloudedi.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.OrderPosition;

import java.util.List;

public class OrderPositionAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private int layout;
    private List<OrderPosition> orderPositions;

    public OrderPositionAdapter(Context context, int layout, List<OrderPosition> articles) {
        super(context, layout, articles);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.orderPositions = articles;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view=inflater.inflate(this.layout,parent,false);

        TextView header=(TextView) view.findViewById(R.id.order_position_name);
        TextView orderPositionItemCode = (TextView) view.findViewById(R.id.order_position_itemcode);
        TextView orderPositionJav = (TextView) view.findViewById(R.id.order_position_jav);
        TextView orderPositionStandart = (TextView) view.findViewById(R.id.order_position_standart);
        TextView orderPositionInfo = (TextView) view.findViewById(R.id.order_position_order_info);
        OrderPosition orderPosition = orderPositions.get(position);

        header.setText(orderPosition.getArticle().getName() + "        " + (orderPosition.getTotal()* (orderPosition.getInitialPrice() > 0? orderPosition.getInitialPrice(): orderPosition.getArticle().getPrice())));
        orderPositionItemCode.setText(orderPosition.getArticle().getItemCode());
        orderPositionJav.setText("JAN - " + orderPosition.getArticle().getJan());
        orderPositionStandart.setText("Standard - " + orderPosition.getArticle().getStandart());
        orderPositionInfo.setText("Separate-"+orderPosition.getSeparate() + " Total-" + orderPosition.getTotal());
        return view;
    }
}
