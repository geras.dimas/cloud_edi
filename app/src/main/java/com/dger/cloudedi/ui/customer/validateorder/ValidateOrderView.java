package com.dger.cloudedi.ui.customer.validateorder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.presenter.CustomerOrderPresenter;
import com.dger.cloudedi.ui.adapters.OrderPositionAdapter;
import com.dger.cloudedi.ui.customer.CustomerActivity;
import com.dger.cloudedi.ui.sellers.SellerActivity;
import com.dger.cloudedi.view.CustomerOrderView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class ValidateOrderView extends MvpAppCompatFragment implements CustomerOrderView {
    @InjectPresenter
    CustomerOrderPresenter customerOrderPresenter;
    TextView sOrderDate, sDeliveryDate, sCategory, sDelivery, sOrderState,sSlipType, sPrimeCost;
    ListView sOrderPositions;
    Button validateButton;
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.seller_fragment_order, container, false);

        sOrderDate = (TextView)root.findViewById(R.id.seller_order_date);
        sDeliveryDate = (TextView)root.findViewById(R.id.seller_delivery_date);
        sDelivery = (TextView)root.findViewById(R.id.seller_delivery);
        sCategory = (TextView)root.findViewById(R.id.seller_category);
        sOrderState = (TextView)root.findViewById(R.id.seller_order_state);
        sSlipType = (TextView)root.findViewById(R.id.seller_slip_type);
        sPrimeCost = (TextView)root.findViewById(R.id.seller_prime_cost);
        sOrderPositions = (ListView) root.findViewById(R.id.seller_order_position_list);
        customerOrderPresenter.setOrder(getArguments());
        validateButton = (Button) root.findViewById(R.id.validate_order);

        validateButton.setText("To pay");

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  customerOrderPresenter.validateOrder();
                //Navigation.findNavController(view).navigate(R.id.action_nav_purchase_order_function_to_nav_purchase_order_entity);
            }
        });
        return root;
    }

    @Override
    public void setOrderDate(String orderDate) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sOrderState.setText(orderDate);
            }
        });
    }

    @Override
    public void setDeliveryDate(String deliveryDate) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sDeliveryDate.setText(deliveryDate);
            }
        });
    }

    @Override
    public void setDelivery(String delivery) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sDelivery.setText(delivery);
            }
        });
    }

    @Override
    public void setCategory(String category) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sCategory.setText(category);
            }
        });
    }

    @Override
    public void setSlipType(String slipType) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sSlipType.setText(slipType);
            }
        });
    }

    @Override
    public void setOrderState(String orderState) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sOrderState.setText(orderState);
            }
        });
    }

    @Override
    public void setPrimeCost(String primeCost) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sPrimeCost.setText(primeCost);
            }
        });
    }

    @Override
    public void setHeader(String header) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CustomerActivity customerActivity = (CustomerActivity) getActivity();
                customerActivity.getSupportActionBar().setTitle(header);
            }
        });
    }

    @Override
    public void setOrderPositionList(List<OrderPosition> orderPositionList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                OrderPositionAdapter orderPositionAdapter = new OrderPositionAdapter(getContext(), R.layout.customer_order_position_item, orderPositionList);
                sOrderPositions.setAdapter(orderPositionAdapter);
                sOrderPositions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        OrderPosition orderPosition = (OrderPosition) sOrderPositions.getItemAtPosition(position);

                        getArguments().putString("orderPositionUUID", orderPosition.getId());
                        Navigation.findNavController(root).navigate(R.id.action_nav_seller_order_to_nav_seller_order_position, getArguments());



                    }
                });
            }
        });
    }

    @Override
    public void setButtonVisibility() {
        getActivity().runOnUiThread(() -> validateButton.setVisibility(View.GONE));
    }
}
