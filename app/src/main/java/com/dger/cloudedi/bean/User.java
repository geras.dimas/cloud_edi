package com.dger.cloudedi.bean;

import com.dger.cloudedi.model.dto.UserDTO;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class User extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;

    @Required
    private String login;

    @Required
    private String email;

    private Shop shop;

    private int roleId;

    public User() {
    }

    public User(UserDTO userDTO) {
        super();
        this.id = userDTO.getId();
        this.name = userDTO.getName();
        this.email = userDTO.getEmail();
        this.login = userDTO.getLogin();
        this.shop = new Shop(userDTO.getShop().getId(), userDTO.getShop().getName());
        this.roleId = userDTO.getUserRole().getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin (String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id!=user.getId()) return false;
        return login.equals(user.getLogin());
    }

}
