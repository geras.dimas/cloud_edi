package com.dger.cloudedi.bean;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OrderPosition extends RealmObject {
    @PrimaryKey
    private String id;

    private int dbId;

    private int dbCase;

    private int separate;

    private int total;

    private int initialPrice;

    private int unitPrice;

    private Article article;

    private Order order;

    public OrderPosition() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public int getDbCase() {
        return dbCase;
    }

    public void setDbCase(int dbCase) {
        this.dbCase = dbCase;
    }

    public int getSeparate() {
        return separate;
    }

    public void setSeparate(int separate) {
        this.separate = separate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(int initialPrice) {
        this.initialPrice = initialPrice;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
