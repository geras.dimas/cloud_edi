package com.dger.cloudedi.bean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SlipType extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;

    public SlipType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SlipType slipType = (SlipType) o;

        if (id != slipType.id) return false;
        return name != null ? name.equals(slipType.name) : slipType.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
