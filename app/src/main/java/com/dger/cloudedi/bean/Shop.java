package com.dger.cloudedi.bean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Shop extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;

    public Shop() {
    }

    public Shop(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
