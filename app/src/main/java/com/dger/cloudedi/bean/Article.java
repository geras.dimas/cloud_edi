package com.dger.cloudedi.bean;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Article extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;

    private String itemCode;

    private String jan;

    private String standart;

    private int qtyPerUnit;

    private int price;

    private int remaining;

    private Shop supplier;

    public Article() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getStandart() {
        return standart;
    }

    public void setStandart(String standart) {
        this.standart = standart;
    }

    public int getQtyPerUnit() {
        return qtyPerUnit;
    }

    public void setQtyPerUnit(int qtyPerUnit) {
        this.qtyPerUnit = qtyPerUnit;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public Shop getSupplier() {
        return supplier;
    }

    public void setSupplier(Shop supplier) {
        this.supplier = supplier;
    }
}
