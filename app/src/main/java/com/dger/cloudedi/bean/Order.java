package com.dger.cloudedi.bean;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

public class Order extends RealmObject {
    @PrimaryKey
    private String id;

    private int dbId;

    private Date orderDate;

    private Date createDate;

    private Date deliveryDate;

    private Category category;

    private Delivery delivery;

    private SlipType slipType;

    private Shop shop;

    private User creator;

    private OrderState orderState;

    private RealmList<OrderPosition> orderPositions;

    public Order() {
        this.id = UUID.randomUUID().toString();
        createDate = new Date();
    }

    public String getId() {
        return this.id;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public SlipType getSlipType() {
        return slipType;
    }

    public void setSlipType(SlipType slipType) {
        this.slipType = slipType;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public RealmList<OrderPosition> getOrderPositions() {
        return orderPositions;
    }

    public void setOrderPositions(RealmList<OrderPosition> orderPositions) {
        this.orderPositions = orderPositions;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
