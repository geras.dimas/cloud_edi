package com.dger.cloudedi.view;

import moxy.MvpView;

public interface SellerHomeVew extends MvpView {
    void setOrderCount(long lOrderCount);
}
