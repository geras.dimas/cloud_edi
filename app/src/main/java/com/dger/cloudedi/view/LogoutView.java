package com.dger.cloudedi.view;

import moxy.MvpView;

public interface LogoutView extends MvpView {

    void  setEmail(String email);
    void setUserName(String userName);
    void transitionLoginView();
}
