package com.dger.cloudedi.view;

import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;

import java.util.List;

import moxy.MvpView;

public interface CustomerOrderListView extends MvpView {
    void setCategory(List<Category> categoryList, Category category);
    void setSlipType(List<SlipType> slipTypeList, SlipType slipType);
    void setDelivery(List<Delivery> deliveryList, Delivery delivery);
    void setOrderState(List<OrderState> orderStateList, OrderState orderState);
    void setOrderList(List<Order> orderList);
}
