package com.dger.cloudedi.view;

import com.dger.cloudedi.bean.Order;

import java.util.List;

import moxy.MvpView;

public interface PurchaseOrderEntity extends MvpView {
    void setOrders(List<Order> orderList);
}
