package com.dger.cloudedi.view;

import moxy.MvpView;

public interface CustomerHomeView extends MvpView {
    void setOrderCount(long lOrderCount);
}
