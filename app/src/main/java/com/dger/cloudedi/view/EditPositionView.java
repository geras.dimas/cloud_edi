package com.dger.cloudedi.view;

import moxy.MvpView;

public interface EditPositionView extends MvpView {
    public void setPositionName(String positionName);
    public void setItemCode(String itemCode);
    public void setJAN(String jan);
    public void setStandard(String standard);
    public void setQtyPerUnit(int qtyPerUnit);
    public void setSupplier(String supplier);
    public void setPrimeCost(int primeCost);
    public void setTotal(int total);
    public void setOperationCase(int operationCase);
    public void setSeparator(int separator);
    public void setInitialPrice(int initialPrice);
}
