package com.dger.cloudedi.view;

import com.dger.cloudedi.bean.OrderPosition;

import java.util.List;

import moxy.MvpView;

public interface SellerOrderView extends MvpView {
    void setOrderDate(String orderDate);
    void setDeliveryDate(String deliveryDate);
    void setDelivery(String delivery);
    void setCategory(String category);
    void setSlipType(String slipType);
    void setOrderState(String orderState);
    void setPrimeCost(String primeCost);
    void setHeader(String header);
    void setOrderPositionList(List<OrderPosition> orderPositionList);
    void setButtonVisibility();
}
