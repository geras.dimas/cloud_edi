package com.dger.cloudedi.view;

import moxy.MvpView;

public interface LoginView extends MvpView {
    void setError(Integer idView, String errorMsg);
    void successVerify(int roleId);
    void savePref(String name, int value);
    void savePref(String name, String value);
    void savePref(String name, boolean value);
}
