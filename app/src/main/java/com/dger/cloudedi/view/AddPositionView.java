package com.dger.cloudedi.view;

import com.dger.cloudedi.bean.Article;

import java.util.List;

import moxy.MvpView;

public interface AddPositionView extends MvpView {
    void setArticleList(List<Article> articleList);
    void showToast(String text);
}
