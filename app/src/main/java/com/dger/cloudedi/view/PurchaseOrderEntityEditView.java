package com.dger.cloudedi.view;

import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.SlipType;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import moxy.MvpView;

public interface PurchaseOrderEntityEditView extends MvpView {
    void setCategory(List<Category> categoryList, Category category);
    void setSlipType(List<SlipType> slipTypeList, SlipType slipType);
    void setDelivery(List<Delivery> deliveryList, Delivery delivery);
    void setOrderDate(Date orderDate);
    void setDeliveryDate(Date deliveryDate);
    void setOrderPositionList(List<OrderPosition> orderPositionList);
    void showToast(String text);
    void blockButton();
}
