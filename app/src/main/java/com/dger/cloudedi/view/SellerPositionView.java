package com.dger.cloudedi.view;

import moxy.MvpView;

public interface SellerPositionView extends MvpView {
    void setName(String name);
    void setItemCode(String itemCode);
    void setJan(String jan);
    void setStandard(String standard);
    void setQty(String qty);
    void setSupplier(String supplier);
    void setPrimeCost(String primeCost);
    void setCase(String dbCase);
    void setSeparate(String separate);
    void setTotal(String total);
    void setInitialPrice(String initialPrice);

}
