package com.dger.cloudedi.presenter;

import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.view.PurchaseOrderEntity;


import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class PurchaseOrderEntityPresenter extends MvpPresenter<PurchaseOrderEntity> {

    private APIService apiService;

    public PurchaseOrderEntityPresenter(){
        apiService = APIService.getInstance();
    }

    public void setOrders(){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            OrderState orderState = realm.where(OrderState.class)
                    .equalTo("name", "Before Processing")
                    .findFirst();
            RealmResults<Order> realmResults = realm.where(Order.class).equalTo("orderState.id", orderState.getId()).findAll();
            List<Order> orderList = realm.copyFromRealm(realmResults);
            realm.close();
            getViewState().setOrders(orderList);
        }).start();
    }

    public void deleteOrderPosition(String orderUUID){

        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order order= realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();

            RealmResults<OrderPosition> orderPositions= realm.where(OrderPosition.class)
                    .equalTo("order.id", orderUUID)
                    .findAll();
            orderPositions.deleteAllFromRealm();
            order.deleteFromRealm();
            realm.commitTransaction();
            realm.close();
            setOrders();
        }).start();
    }
}
