package com.dger.cloudedi.presenter;

import com.dger.cloudedi.App;
import com.dger.cloudedi.bean.Article;
import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.Shop;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.bean.User;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.model.dto.ArticleDTO;
import com.dger.cloudedi.model.dto.OrderPositionGetDTO;
import com.dger.cloudedi.model.dto.UserDTO;
import com.dger.cloudedi.view.CustomerView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmList;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class CustomerPresenter extends MvpPresenter<CustomerView> {

    private APIService apiService;

    private App app;
    public CustomerPresenter() {
        apiService = APIService.getInstance();
        app = App.get();
    }


    public void getOrders() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            User user = realm.where(User.class).equalTo("id", app.getCloudUserId()).findFirst();
            apiService.getOrdersForCustomer(user.getShop().getId()).subscribe(orderPositionGetDTOS -> setOrders(orderPositionGetDTOS), error -> System.out.println(error));
            realm.close();
        }).start();
    }

    private void setOrders(List<OrderPositionGetDTO> orderPositionGetDTOS){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            orderPositionGetDTOS.forEach(order -> {
                Order orderRealm = realm.where(Order.class).equalTo("dbId", order.getOrder().getId()).findFirst();
                if (orderRealm == null) {
                    orderRealm = new Order();
                    orderRealm.setDbId(order.getOrder().getId());
                    RealmList<OrderPosition> orderPositions = new RealmList();
                    orderRealm.setOrderPositions(orderPositions);
                }
                Category categoryRealm = realm.where(Category.class).equalTo("id", order.getOrder().getCategoryId()).findFirst();
                Delivery deliveryRealm = realm.where(Delivery.class).equalTo("id", order.getOrder().getDeliveryId()).findFirst();
                SlipType slipTypeRealm = realm.where(SlipType.class).equalTo("id", order.getOrder().getSlipTypeId()).findFirst();
                OrderState orderStateRealm = realm.where(OrderState.class).equalTo("id", order.getOrder().getOrderStateId()).findFirst();

                orderRealm.setOrderState(orderStateRealm);
                orderRealm.setDelivery(deliveryRealm);
                orderRealm.setSlipType(slipTypeRealm);
                orderRealm.setCategory(categoryRealm);
                orderRealm.setDeliveryDate(order.getOrder().getDeliveryDate());
                orderRealm.setOrderDate(order.getOrder().getOrderDate());
                orderRealm.setCreateDate(order.getOrder().getCreateDate());

                UserDTO userDTO = order.getOrder().getUser();
                User userRealm = realm.where(User.class).equalTo("id", userDTO.getId()).findFirst();

                if (userRealm == null) {
                    userRealm = new User();
                    userRealm.setId(userDTO.getId());
                    userRealm.setName(userDTO.getName());
                    userRealm.setEmail(userDTO.getEmail());
                    userRealm.setLogin(userDTO.getLogin());
                    userRealm.setRoleId(userDTO.getUserRole().getId());
                }

                Shop shopRealm = realm.where(Shop.class).equalTo("id", userDTO.getShop().getId()).findFirst();
                if (shopRealm == null) {
                    shopRealm = new Shop();
                    shopRealm.setId(userDTO.getShop().getId());
                    shopRealm.setName(userDTO.getShop().getName());
                }
                realm.copyToRealmOrUpdate(shopRealm);
                userRealm.setShop(shopRealm);
                realm.copyToRealmOrUpdate(userRealm);

                orderRealm.setCreator(userRealm);

                orderRealm.setShop(shopRealm);

                realm.copyToRealmOrUpdate(orderRealm);

                ArticleDTO articleDTO = order.getArticle();

                Article articleRealm = realm.where(Article.class).equalTo("id", articleDTO.getId()).findFirst();

                if (articleRealm == null) {
                    articleRealm = new Article();
                    articleRealm.setId(articleDTO.getId());
                }
                articleRealm.setName(articleDTO.getName());
                articleRealm.setRemaining(articleDTO.getRemaining());
                articleRealm.setPrice(articleDTO.getPrice());
                articleRealm.setQtyPerUnit(articleDTO.getQtyPerUnit());
                articleRealm.setItemCode(articleDTO.getItemCode());
                articleRealm.setJan(articleDTO.getJan());
                articleRealm.setStandart(articleDTO.getStandart());

                Shop shopArticleRealm = realm.where(Shop.class).equalTo("id", articleDTO.getSupplierId()).findFirst();
                if (shopArticleRealm == null) {
                    shopArticleRealm = new Shop();
                    shopArticleRealm.setId(articleDTO.getShop().getId());
                    shopArticleRealm.setName(articleDTO.getShop().getName());
                }
                realm.copyToRealmOrUpdate(shopArticleRealm);
                articleRealm.setSupplier(shopArticleRealm);

                realm.copyToRealmOrUpdate(articleRealm);

                OrderPosition orderPositionRealm = realm.where(OrderPosition.class).equalTo("dbId", order.getId()).findFirst();

                if (orderPositionRealm == null) {
                    orderPositionRealm = new OrderPosition();
                    orderPositionRealm.setDbId(order.getId());
                }
                orderPositionRealm.setSeparate(order.getSeparate());
                orderPositionRealm.setDbCase(order.getDbCase());
                orderPositionRealm.setTotal(order.getTotal());
                orderPositionRealm.setSeparate(order.getSeparate());
                orderPositionRealm.setInitialPrice(order.getInitialPrice());
                orderPositionRealm.setOrder(orderRealm);
                orderPositionRealm.setArticle(articleRealm);

                realm.copyToRealmOrUpdate(orderPositionRealm);
                orderRealm.getOrderPositions().add(orderPositionRealm);

                Set<OrderPosition> orderPositionSet = new HashSet<>();
                orderPositionSet.addAll(orderRealm.getOrderPositions());
                orderRealm.getOrderPositions().clear();
                orderRealm.getOrderPositions().addAll(orderPositionSet);

                realm.copyToRealmOrUpdate(orderRealm);

            });
            realm.commitTransaction();
            realm.close();
        }).start();
    }
}
