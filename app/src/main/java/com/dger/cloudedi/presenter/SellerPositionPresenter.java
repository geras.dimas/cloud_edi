package com.dger.cloudedi.presenter;

import android.os.Bundle;

import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.view.SellerPositionView;

import io.realm.Realm;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class SellerPositionPresenter extends MvpPresenter<SellerPositionView> {

    public void setPosition(Bundle savedInstanceState){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            String uuid = savedInstanceState.getString("orderPositionUUID");
            OrderPosition orderPositionRealm;
            orderPositionRealm = realm.where(OrderPosition.class)
                    .equalTo("id", uuid)
                    .findFirst();
            orderPositionRealm = realm.copyFromRealm(orderPositionRealm);
            getViewState().setCase(orderPositionRealm.getDbCase()>0?Integer.toString(orderPositionRealm.getDbCase()):"0");
            getViewState().setName(orderPositionRealm.getArticle().getName());
            getViewState().setItemCode(orderPositionRealm.getArticle().getItemCode());
            getViewState().setInitialPrice(Integer.toString(orderPositionRealm.getInitialPrice()>0?orderPositionRealm.getInitialPrice():orderPositionRealm.getArticle().getPrice()));
            getViewState().setJan(orderPositionRealm.getArticle().getJan());
            getViewState().setQty(Integer.toString(orderPositionRealm.getArticle().getQtyPerUnit()));
            getViewState().setStandard(orderPositionRealm.getArticle().getStandart());
            getViewState().setTotal(Integer.toString(orderPositionRealm.getTotal()));
            getViewState().setSeparate(Integer.toString(orderPositionRealm.getSeparate()));
            getViewState().setSupplier(orderPositionRealm.getArticle().getSupplier().getName());
            getViewState().setPrimeCost((Integer.toString(orderPositionRealm.getTotal()*(orderPositionRealm.getInitialPrice()>0?orderPositionRealm.getInitialPrice():orderPositionRealm.getArticle().getPrice()))));
            realm.close();
        }).start();
    }
}
