package com.dger.cloudedi.presenter;

import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.view.SellerOrderListView;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class SellerOrderListPresenter extends MvpPresenter<SellerOrderListView> {

    private int orderStateId = 2, slipTypeId = 0, deliveryId =0, categoryId = 0;
    private String shopName = "";

    public SellerOrderListPresenter() {
    }

    public void setOrders() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();

            RealmQuery<Order> queryOrder = realm.where(Order.class);

            if(orderStateId > 0){
                queryOrder.equalTo("orderState.id", orderStateId);
            }
            if(slipTypeId > 0){
                queryOrder .equalTo("slipType.id", slipTypeId);
            }
            if(deliveryId > 0){
                queryOrder.equalTo("delivery.id", deliveryId);
            }
            if(categoryId > 0){
                queryOrder.equalTo("category.id", categoryId);
            }

            RealmResults<Order> realmResults = queryOrder.like("shop.name", "*" + shopName + "*", Case.INSENSITIVE)
                    .findAll();
            List<Order> orderList = realm.copyFromRealm(realmResults);

            getViewState().setOrderList(orderList);
            realm.close();
        }).start();
    }

    public void updateCategory(Category category) {
        categoryId = category.getId();
        //setOrders();
    }

    public void updateDelivery(Delivery delivery) {
        deliveryId = delivery.getId();
        //setOrders();
    }

    public void updateSlipType(SlipType slipType) {
        slipTypeId = slipType.getId();
        //setOrders();
    }

    public void updateOrderState(OrderState orderState) {
        orderStateId = orderState.getId();
        //setOrders();
    }

    public void setCategories() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Category> realmResults = realm.where(Category.class).findAll().sort("id");
            List<Category> categoryList = realm.copyFromRealm(realmResults);

            Category categoryRealm = realm.where(Category.class).equalTo("id", categoryId).findFirst();
            Category category = realm.copyFromRealm(categoryRealm);
            getViewState().setCategory(categoryList, category);
            realm.close();


        }).start();
    }

    public void setSlipTypes() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<SlipType> realmResults = realm.where(SlipType.class).findAll().sort("id");
            List<SlipType> slipTypeList = realm.copyFromRealm(realmResults);
            SlipType slipTypeRealm = realm.where(SlipType.class).equalTo("id", slipTypeId).findFirst();
            SlipType slipType = realm.copyFromRealm(slipTypeRealm);
            getViewState().setSlipType(slipTypeList, slipType);
            realm.close();
        }).start();
    }

    public void setDeliveries() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Delivery> realmResults = realm.where(Delivery.class).findAll().sort("id");
            List<Delivery> deliveryList = realm.copyFromRealm(realmResults);
            Delivery deliveryRealm = realm.where(Delivery.class).equalTo("id", deliveryId).findFirst();
            Delivery delivery = realm.copyFromRealm(deliveryRealm);
            getViewState().setDelivery(deliveryList, delivery);
            realm.close();
        }).start();
    }

    public void setOrderStates() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<OrderState> realmResults = realm.where(OrderState.class).findAll().sort("id");
            List<OrderState> orderStates = realm.copyFromRealm(realmResults);
            OrderState orderStateRealm = realm.where(OrderState.class).equalTo("id", orderStateId).findFirst();
            OrderState orderState = realm.copyFromRealm(orderStateRealm);
            getViewState().setOrderState(orderStates, orderState);
            realm.close();
        }).start();
    }

    public void init(){
        setCategories();
        setDeliveries();
        setOrderStates();
        setSlipTypes();
        setOrders();
    }
}
