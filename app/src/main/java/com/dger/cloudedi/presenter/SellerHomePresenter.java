package com.dger.cloudedi.presenter;

import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.view.SellerHomeVew;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class SellerHomePresenter extends MvpPresenter<SellerHomeVew> {

    RealmResults<Order> orders;
    private OrderedRealmCollectionChangeListener<RealmResults<Order>> changeListener;

    public SellerHomePresenter() {
        Realm realm = Realm.getDefaultInstance();
        OrderState orderState = realm.where(OrderState.class).equalTo("name", "Instructed").findFirst();

        orders = realm.where(Order.class).equalTo("orderState.id", orderState.getId()).findAll();
        changeListener = (collection, changeSet) -> {
            // `null`  means the async query returns the first time.
            if (changeSet == null) {
                return;
            }
            // For deletions, the adapter has to be notified in reverse order.
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
            }
            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                setOrderCount();
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                System.out.println("test");
            }
        };

        orders.addChangeListener(changeListener);
    }


    public void setOrderCount(){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            long orderCount =0;

            OrderState orderState = realm.where(OrderState.class).equalTo("name", "Instructed").findFirst();
            orderCount = realm.where(Order.class).equalTo("orderState.id", orderState.getId()).count();

            getViewState().setOrderCount(orderCount);

        }).start();
    }
}
