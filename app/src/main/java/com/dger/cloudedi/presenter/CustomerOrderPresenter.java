package com.dger.cloudedi.presenter;

import android.os.Bundle;

import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.model.dto.ValidateDTO;
import com.dger.cloudedi.view.CustomerOrderView;

import java.text.SimpleDateFormat;
import java.util.List;

import io.realm.Realm;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class CustomerOrderPresenter extends MvpPresenter<CustomerOrderView> {
    SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy/MM/dd" );
    private APIService apiService;
    private int orderId;

    public CustomerOrderPresenter() {
        apiService = APIService.getInstance();
    }
    public void setOrder(Bundle savedInstanceState) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            String uuid = savedInstanceState.getString("orderUUID");
            Order order = realm.where(Order.class)
                    .equalTo("id", uuid)
                    .findFirst();

            orderId = order.getDbId();
            getViewState().setCategory(order.getCategory().getName());
            getViewState().setDelivery(order.getDelivery().getName());
            getViewState().setDeliveryDate(dateFormat.format(order.getDeliveryDate()));
            getViewState().setOrderDate(dateFormat.format(order.getOrderDate()));
            getViewState().setOrderState(order.getOrderState().getName());
            getViewState().setSlipType(order.getSlipType().getName());
            int price = order.getOrderPositions().stream().mapToInt(orderPosition -> orderPosition.getTotal() * (orderPosition.getInitialPrice()>0 ? orderPosition.getInitialPrice() : orderPosition.getArticle().getPrice())).sum();
            getViewState().setPrimeCost(Integer.toString(price));
            getViewState().setHeader("Order for " + order.getOrderPositions().get(0).getArticle().getSupplier().getName());
            List<OrderPosition> orderPositionList = realm.copyFromRealm(order.getOrderPositions());
            getViewState().setOrderPositionList(orderPositionList);
        }).start();
    }

    public void validateOrder(){
        apiService.validateOrder(orderId).subscribe(validateDTO -> setOrderCategory(validateDTO), error -> System.out.println(error));
    }

    private void setOrderCategory(ValidateDTO validateDTO){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order order = realm.where(Order.class)
                    .equalTo("dbId", orderId)
                    .findFirst();

            OrderState orderState = realm.where(OrderState.class)
                    .equalTo("id", 3)
                    .findFirst();
            order.setOrderState(orderState);

            realm.copyToRealmOrUpdate(order);
            getViewState().setOrderState(orderState.getName());
            getViewState().setButtonVisibility();
            realm.commitTransaction();
        }).start();
    }
}
