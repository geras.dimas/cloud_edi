package com.dger.cloudedi.presenter;

import android.os.Bundle;

import com.dger.cloudedi.bean.Article;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.Shop;
import com.dger.cloudedi.model.WebSocketService;
import com.dger.cloudedi.view.AddPositionView;

import java.util.List;

import io.realm.Case;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class AddPositionPresenter extends MvpPresenter<AddPositionView>{

    String search;
    private WebSocketService wss;
    RealmResults<Article> articles;
    private OrderedRealmCollectionChangeListener<RealmResults<Article>> changeListener;
    private String orderUUID;
    public AddPositionPresenter(){
        wss = WebSocketService.getInstance();
        Realm realm = Realm.getDefaultInstance();
        articles = realm.where(Article.class).findAll();
        changeListener = (collection, changeSet) -> {
            // `null`  means the async query returns the first time.
            if (changeSet == null) {
                return;
            }
            // For deletions, the adapter has to be notified in reverse order.
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
            }
            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                getArticlesList();
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                System.out.println("test");
            }
        };

    }


    public void searchArticle(String search){

            Realm realm = Realm.getDefaultInstance();
            this.search = search;
            String realmSearch = "*"+this.search+"*";
            articles = realm.where(Article.class).like("name",realmSearch, Case.INSENSITIVE)
                    .or().like("itemCode", realmSearch, Case.INSENSITIVE)
                    .or().like("jan", realmSearch, Case.INSENSITIVE)
                   .findAll();
            Article article = realm.where(Article.class).equalTo("id",1).findFirst();
            articles.removeAllChangeListeners();
            articles.addChangeListener(changeListener);
            List<Article> articleList = realm.copyFromRealm(articles);
            setArticlesList(articleList);
            wss.sendMessage("{\"type\":\"findArticle\", \"search\":\"" + search + "\"}");
            realm.close();

    }

    public void setArticlesList(List<Article> articlesList){
        getViewState().setArticleList(articlesList);
    }

    public void getArticlesList(){

        Realm realm = Realm.getDefaultInstance();
        List<Article>  articleList = realm.copyFromRealm(articles);
        realm.close();
        setArticlesList(articleList);
    }

    public void setOrder(Bundle savedInstanceState) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            String uuid = savedInstanceState.getString("orderUUID");
            Order order;
            order = realm.where(Order.class)
                    .equalTo("id", uuid)
                    .findFirst();
            orderUUID = order.getId();
            realm.close();
        }).start();
    }

    public void AddArticleInOrder(Article article){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();

            Order order;
            order = realm.where(Order.class)
                    .equalTo("id", this.orderUUID)
                    .findFirst();
            OrderPosition orderPosition = realm.where(OrderPosition.class)
                    .equalTo("order.id", order.getId())
                    .equalTo("article.id", article.getId())
                    .findFirst();
            if(orderPosition == null) {

                orderPosition = new OrderPosition();
                orderPosition.setArticle(article);
                orderPosition.setOrder(order);
                orderPosition.setInitialPrice(article.getPrice());
                orderPosition.setSeparate(0);
                orderPosition.setUnitPrice(0);
                orderPosition.setTotal(1);
                orderPosition.setDbCase(0);

                realm.copyToRealmOrUpdate(orderPosition);
                order.getOrderPositions().add(orderPosition);
                realm.copyToRealmOrUpdate(order);

                getViewState().showToast("\"" + article.getName() + "\" has been added to the order");
            }
            else{
                getViewState().showToast("\"" + article.getName() + "\" had been added to the order");
            }
            realm.commitTransaction();
            realm.close();
        }).start();
    }
}
