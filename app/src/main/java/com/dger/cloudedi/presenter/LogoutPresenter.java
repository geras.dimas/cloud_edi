package com.dger.cloudedi.presenter;

import android.content.SharedPreferences;

import com.dger.cloudedi.App;
import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.bean.User;
import com.dger.cloudedi.view.LogoutView;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class LogoutPresenter extends MvpPresenter <LogoutView> {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private void LogoutPresenter(){

    }
    public void initParams() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            prefs = App.get().getProfileSettings();
            int userid = prefs.getInt("USER_ID", 1);
            User userRealm = realm.where(User.class)
                    .equalTo("id", userid)
                    .findFirst();
            User user = realm.copyFromRealm(userRealm);
            getViewState().setEmail(user.getEmail());
            getViewState().setUserName(user.getName());
            realm.close();
        }).start();
    }
    public void logout(){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            prefs = App.get().getProfileSettings();
            editor = prefs.edit();
            realm.beginTransaction();

            RealmResults<OrderPosition> orderPositions = realm.where(OrderPosition.class).findAll();
            RealmResults<Order> orders = realm.where(Order.class).findAll();
            RealmResults<User> users = realm.where(User.class).findAll();
            orderPositions.deleteAllFromRealm();
            orders.deleteAllFromRealm();
            users.deleteAllFromRealm();

            Category category = realm.where(Category.class)
                    .equalTo("id", 0)
                    .findFirst();

            if(category != null){
                category.deleteFromRealm();
            }

            SlipType slipType = realm.where(SlipType.class)
                    .equalTo("id", 0)
                    .findFirst();

            if (slipType != null){
                slipType.deleteFromRealm();
            }

            Delivery delivery = realm.where(Delivery.class)
                    .equalTo("id", 0)
                    .findFirst();

            if(delivery != null) {
                delivery.deleteFromRealm();
            }

            OrderState orderState = realm.where(OrderState.class)
                    .equalTo("id", 0)
                    .findFirst();

            if(orderState != null) {
                orderState.deleteFromRealm();
            }

            realm.commitTransaction();
            editor.clear();
            editor.commit();
            getViewState().transitionLoginView();
            realm.close();
        }).start();
    }
}
