package com.dger.cloudedi.presenter;

import android.os.Bundle;

import com.dger.cloudedi.R;
import com.dger.cloudedi.bean.Category;
import com.dger.cloudedi.bean.Delivery;
import com.dger.cloudedi.bean.Order;
import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.bean.OrderState;
import com.dger.cloudedi.bean.SlipType;
import com.dger.cloudedi.bean.User;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.model.dto.CategoryDTO;
import com.dger.cloudedi.model.dto.CreateDTO;
import com.dger.cloudedi.model.dto.CreatePositionDTO;
import com.dger.cloudedi.model.dto.DeliveryDTO;
import com.dger.cloudedi.model.dto.SlipTypeDTO;
import com.dger.cloudedi.view.PurchaseOrderEntityEditView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class PurchaseOrderEntityEditPresenter extends MvpPresenter<PurchaseOrderEntityEditView> {
    private APIService apiService;
    private Order order;
    private String orderUUID;
    public PurchaseOrderEntityEditPresenter(){
        apiService = APIService.getInstance();
    }

    public void setCategories() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Category> realmResults = realm.where(Category.class).findAll();
            List<Category> categoryList = realm.copyFromRealm(realmResults);
            Order orderRealm = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            Category categoryRealm = orderRealm.getCategory();
            Category category = realm.copyFromRealm(categoryRealm);
            getViewState().setCategory(categoryList, category);
            realm.close();


        }).start();
    }

    public void setSlipTypes() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<SlipType> realmResults = realm.where(SlipType.class).findAll();
            List<SlipType> slipTypeList = realm.copyFromRealm(realmResults);
            Order orderRealm = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            SlipType slipTypeRealm = orderRealm.getSlipType();
            SlipType slipType = realm.copyFromRealm(slipTypeRealm);
            realm.close();
            getViewState().setSlipType(slipTypeList, slipType);
        }).start();
    }

    public void setDeliveries() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Delivery> realmResults = realm.where(Delivery.class).findAll();
            List<Delivery> deliveryList = realm.copyFromRealm(realmResults);
            Order orderRealm = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            Delivery deliveryRealm = orderRealm.getDelivery();
            Delivery delivery = realm.copyFromRealm(deliveryRealm);
            realm.close();
            getViewState().setDelivery(deliveryList, delivery);
        }).start();
    }

    public void setOrder(Bundle savedInstanceState) {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            if (savedInstanceState != null){
                String uuid = savedInstanceState.getString("orderUUID");
                order = realm.where(Order.class)
                        .equalTo("id", uuid)
                        .findFirst();
                this.orderUUID = order.getId();
            }
            else {
                realm.beginTransaction();
                order = new Order();
                Category category = realm.where(Category.class)
                        .equalTo("name", "Unspecify")
                        .findFirst();
                User user = realm.where(User.class)
                        .findFirst();
                Delivery delivery = realm.where(Delivery.class)
                        .equalTo("name", "Not Specified")
                        .findFirst();
                OrderState orderState = realm.where(OrderState.class)
                        .equalTo("name", "Before Processing")
                        .findFirst();
                SlipType slipType = realm.where(SlipType.class)
                        .equalTo("name", "Purchase Slip")
                        .findFirst();
                order.setCategory(category);
                order.setDelivery(delivery);
                order.setOrderState(orderState);
                order.setSlipType(slipType);
                Calendar cal = Calendar.getInstance();
                Date orderDate = cal.getTime();
                cal.add(Calendar.DATE, 1);
                Date deliveryDate = cal.getTime();
                order.setOrderDate(orderDate);
                order.setDeliveryDate(deliveryDate);
                order.setCreator(user);
                order.setShop(user.getShop());

                realm.copyToRealmOrUpdate(order);
                this.orderUUID = order.getId();
                realm.commitTransaction();
            }
            initParam();
        }).start();
    }

    private void initParam(){
        getViewState().setDeliveryDate(order.getDeliveryDate());
        getViewState().setOrderDate(order.getOrderDate());
        setCategories();
        setSlipTypes();
        setDeliveries();
        setOrderPositions();
    }

    public String getOrderUUID(){
            return this.orderUUID;
    }

    public void setOrderPositions() {
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<OrderPosition> realmResults = realm.where(OrderPosition.class)
                    .equalTo("order.id", orderUUID)
                    .findAll();
            List<OrderPosition> orderPositions = realm.copyFromRealm(realmResults);
            realm.close();
            getViewState().setOrderPositionList(orderPositions);
        }).start();
    }

    public void updateCategory(Category category){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderUpdate = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            Category categoryRealm = realm.where(Category.class)
                    .equalTo("id", category.getId())
                    .findFirst();

            orderUpdate.setCategory(categoryRealm);
            realm.copyToRealmOrUpdate(orderUpdate);
            realm.commitTransaction();
            realm.close();
        }).start();
    }
    public void updateSlipType(SlipType slipType){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderUpdate = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            SlipType slipTypeRealm = realm.where(SlipType.class)
                    .equalTo("id", slipType.getId())
                    .findFirst();

            orderUpdate.setSlipType(slipTypeRealm);
            realm.copyToRealmOrUpdate(orderUpdate);
            realm.commitTransaction();
            realm.close();
        }).start();
    }
    public void updateDelivery(Delivery delivery){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderUpdate = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            Delivery deliveryRealm = realm.where(Delivery.class)
                    .equalTo("id", delivery.getId())
                    .findFirst();

            orderUpdate.setDelivery(deliveryRealm);
            realm.copyToRealmOrUpdate(orderUpdate);
            realm.commitTransaction();
            realm.close();
        }).start();
    }

    public void updateDeliveryDate(Date deliveryDate){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderUpdate = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            orderUpdate.setDeliveryDate(deliveryDate);
            realm.copyToRealmOrUpdate(orderUpdate);
            realm.commitTransaction();
            realm.close();
        }).start();
    }
    public void updateOrderDate(Date orderDate){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderUpdate = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            orderUpdate.setOrderDate(orderDate);
            realm.copyToRealmOrUpdate(orderUpdate);
            realm.commitTransaction();
            realm.close();
        }).start();
    }

    public void deleteOrderPosition(String orderPositionUUID){

        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            OrderPosition orderPosition = realm.where(OrderPosition.class)
                    .equalTo("id", orderPositionUUID)
                    .findFirst();
            if((Integer)orderPosition.getDbId() != null && orderPosition.getDbId() > 0){
                apiService.deleteOrderPosition(orderPosition.getDbId()).subscribe(deleteOrderPositionDTO -> deleteOrderPositionByDBId(deleteOrderPositionDTO.getId()), error -> System.out.println(error));
                return;
            }
            orderPosition.deleteFromRealm();
            realm.commitTransaction();
            realm.close();
            setOrderPositions();
        }).start();
    }

    public void deleteOrderPositionByDBId(int dbId){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            OrderPosition orderPosition = realm.where(OrderPosition.class)
                    .equalTo("dbId", dbId)
                    .findFirst();

            orderPosition.deleteFromRealm();
            realm.commitTransaction();
            realm.close();
            setOrderPositions();
        }).start();
    }

    public void confirmOrder(){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            long positionCount = realm.where(OrderPosition.class)
                    .equalTo("order.id", orderUUID)
                    .count();
            if ( positionCount == 0) {
                getViewState().showToast("You cannot send an empty order");
                realm.close();
                return;
            }
            Order orderRealm = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            OrderState orderState = realm.where(OrderState.class).equalTo("name", "Instructed").findFirst();
            orderRealm.setOrderState(orderState);
            orderRealm = realm.copyToRealmOrUpdate(orderRealm);
            realm.commitTransaction();
            Order order = realm.copyFromRealm(orderRealm);
            apiService.createOrder(order).subscribe(createDTO-> setOrderIdAndSendOrderPositions(createDTO), error -> System.out.println(error));
            realm.close();
        }).start();
    }

    private void setOrderIdAndSendOrderPositions(CreateDTO createDTO){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Order orderRealm = realm.where(Order.class)
                    .equalTo("id", orderUUID)
                    .findFirst();
            orderRealm.setDbId(createDTO.getId());
            realm.copyToRealmOrUpdate(orderRealm);

            RealmResults<OrderPosition> realmResults = realm.where(OrderPosition.class)
                    .equalTo("order.id", orderUUID)
                    .findAll();

            List<OrderPosition> orderPositions = realm.copyFromRealm(realmResults);

            realm.commitTransaction();
            getViewState().blockButton();
            orderPositions.forEach(orderPosition -> {
                apiService.createOrderPosition(orderPosition).subscribe(createPositionDTO-> setOrderPosition(createPositionDTO), error -> System.out.println(error));
            });

            realm.close();
        }).start();
    }

    private void setOrderPosition(CreatePositionDTO createPositionDTO){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            OrderPosition orderPosition = realm.where(OrderPosition.class)
                    .equalTo("id", createPositionDTO.getUuid())
                    .findFirst();
            orderPosition.setDbId(createPositionDTO.getId());
            realm.copyToRealmOrUpdate(orderPosition);
            realm.commitTransaction();
            realm.close();
        }).start();
    }
}
