package com.dger.cloudedi.presenter;


import com.dger.cloudedi.bean.Shop;
import com.dger.cloudedi.bean.User;
import com.dger.cloudedi.model.APIService;
import com.dger.cloudedi.model.dto.UserDTO;
import com.dger.cloudedi.view.LoginView;
import com.dger.cloudedi.R;

import io.realm.Realm;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class LoginPresenter  extends MvpPresenter<LoginView> {
    private APIService apiService;
    private Realm realm;

    public LoginPresenter(){
        apiService = APIService.getInstance();
    }

    public void loggin(String login, String password) {
        apiService.login(login, password).subscribe(user -> setUser(user), error -> getViewState().setError(R.id.login, error.getMessage()));
    }

    private void setUser(UserDTO userDTO){
        if(userDTO == null){
            getViewState().setError(R.id.login,"invalid username or password");
            return;
        }
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        User userRealm = realm.where(User.class).equalTo("id", userDTO.getId()).findFirst();
        if(userRealm == null){
            userRealm = new User();
            userRealm.setId(userDTO.getId());
            userRealm.setName(userDTO.getName());
            userRealm.setEmail(userDTO.getEmail());
            userRealm.setLogin(userDTO.getLogin());
            userRealm.setRoleId(userDTO.getUserRole().getId());
        }

        Shop shopRealm = realm.where(Shop.class).equalTo("id", userDTO.getShop().getId()).findFirst();
        if(shopRealm == null) {
            shopRealm = new Shop();
            shopRealm.setId(userDTO.getShop().getId());
            shopRealm.setName(userDTO.getShop().getName());
        }
        realm.copyToRealmOrUpdate(shopRealm);
        userRealm.setShop(shopRealm);

        getViewState().savePref("USER_AUTH", true);
        getViewState().savePref("USER_ID", userRealm.getId());
        getViewState().savePref("USER_ROLE_ID", userRealm.getRoleId());

        realm.copyToRealmOrUpdate(userRealm);
        realm.commitTransaction();
        getViewState().successVerify(userDTO.getUserRole().getId());
    }
}
