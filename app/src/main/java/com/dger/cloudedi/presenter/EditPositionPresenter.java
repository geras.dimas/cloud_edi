package com.dger.cloudedi.presenter;

import android.os.Bundle;

import com.dger.cloudedi.bean.OrderPosition;
import com.dger.cloudedi.view.EditPositionView;

import io.realm.Realm;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class EditPositionPresenter extends MvpPresenter<EditPositionView> {
    private String orderPositionUUID;
    private OrderPosition orderPosition;
    public EditPositionPresenter() {
    }

    public void setOrderPosition(Bundle bundle){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            String uuid = bundle.getString("orderPositionUUID");
            OrderPosition orderPositionRealm;
            orderPositionRealm = realm.where(OrderPosition.class)
                    .equalTo("id", uuid)
                    .findFirst();
            orderPositionUUID = orderPositionRealm.getId();
            orderPosition = realm.copyFromRealm(orderPositionRealm);
            realm.close();
            initParam();
        }).start();
    }

    public void initParam(){
        getViewState().setTotal(orderPosition.getTotal());
        getViewState().setPositionName(orderPosition.getArticle().getName());
        getViewState().setInitialPrice(orderPosition.getInitialPrice());
        getViewState().setJAN(orderPosition.getArticle().getJan());
        getViewState().setItemCode(orderPosition.getArticle().getItemCode());
        getViewState().setQtyPerUnit(orderPosition.getArticle().getQtyPerUnit());
        getViewState().setOperationCase(orderPosition.getDbCase());
        getViewState().setSeparator(orderPosition.getSeparate());
        getViewState().setSupplier(orderPosition.getArticle().getSupplier().getName());
        getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
    }

    public void updateTotal(int total){
        new Thread(() -> {
            if( orderPosition.getTotal() != total) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                OrderPosition orderPositionRealm = realm.where(OrderPosition.class)
                        .equalTo("id", orderPositionUUID)
                        .findFirst();
                orderPositionRealm.setTotal(total);
                if ((orderPositionRealm.getDbCase() * orderPositionRealm.getArticle().getQtyPerUnit()) > total) {
                    orderPositionRealm.setDbCase(total / orderPositionRealm.getArticle().getQtyPerUnit());
                    getViewState().setOperationCase(total / orderPositionRealm.getArticle().getQtyPerUnit());
                }
                int newSeparate = total - (orderPositionRealm.getDbCase() * orderPositionRealm.getArticle().getQtyPerUnit());
                if (orderPositionRealm.getSeparate() != newSeparate) {
                    orderPositionRealm.setSeparate(newSeparate);
                    getViewState().setSeparator(newSeparate);
                }
                realm.copyToRealmOrUpdate(orderPositionRealm);
                realm.commitTransaction();
                orderPosition = realm.copyFromRealm(orderPositionRealm);
                getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
                realm.close();
            }
        }).start();
    }
    public void updateSeparator(int separator){
        new Thread(() -> {
            if ( orderPosition.getSeparate() != separator) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                OrderPosition orderPositionRealm = realm.where(OrderPosition.class)
                        .equalTo("id", orderPositionUUID)
                        .findFirst();
                orderPositionRealm.setSeparate(separator);
                int newTotal =(orderPositionRealm.getDbCase() * orderPositionRealm.getArticle().getQtyPerUnit()) + separator;
                if (orderPositionRealm.getTotal() != newTotal) {
                    orderPositionRealm.setTotal(newTotal);
                    getViewState().setTotal(newTotal);
                }
                realm.copyToRealmOrUpdate(orderPositionRealm);
                realm.commitTransaction();
                orderPosition = realm.copyFromRealm(orderPositionRealm);
                getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
                realm.close();
            }
        }).start();
    }
    public void updateInitialPrice(int initPrice){
        new Thread(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            OrderPosition orderPositionRealm = realm.where(OrderPosition.class)
                    .equalTo("id", orderPositionUUID)
                    .findFirst();
            orderPositionRealm.setInitialPrice(initPrice);
            realm.copyToRealmOrUpdate(orderPositionRealm);
            realm.commitTransaction();
            orderPosition = realm.copyFromRealm(orderPositionRealm);
            getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
            realm.close();

        }).start();
    }
    public void updateOperationCase(int operationCase){
        new Thread(() -> {
            if(orderPosition.getDbCase() != operationCase) {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                OrderPosition orderPositionRealm = realm.where(OrderPosition.class)
                        .equalTo("id", orderPositionUUID)
                        .findFirst();
                orderPositionRealm.setDbCase(operationCase);
                int newTotal =(operationCase * orderPositionRealm.getArticle().getQtyPerUnit()) + orderPositionRealm.getSeparate();
                if(orderPositionRealm.getTotal() != newTotal) {
                    orderPositionRealm.setTotal(newTotal);
                    getViewState().setTotal(newTotal);
                }
                realm.copyToRealmOrUpdate(orderPositionRealm);
                realm.commitTransaction();
                getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
                orderPosition = realm.copyFromRealm(orderPositionRealm);
                getViewState().setPrimeCost(orderPosition.getInitialPrice() * orderPosition.getTotal());
                realm.close();
            }
        }).start();
    }

}
